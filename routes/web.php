<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/*guest route*/

Route::group([''],function(){
	Route::get('/test',function(){
		return view('test');
	});

	//Route::get('/createuser','HomeController@createUser');

	Route::get('/', function () {
	    return view('welcome');
	})->name('landingPage');

	Route::get('/get-summerize','Auth\SurveyController@getSummerize');

	Route::get('/survey-form',"Auth\SurveyController@surveyForm")->name('surveyForm');		

	Route::get('/admin-panel',"Auth\LoginController@login")->name('login');	

	Route::get('/forgot-password',"Auth\LoginController@forgotPassword")->name('forgotPassword');	

	Route::post('/login', "Auth\LoginController@authenticate")->name('loginPost');

	Route::post('/survey-post','Auth\SurveyController@surveyPost')->name('surveyPost');

	Route::get('/survey-mark-read/{id}','Auth\SurveyController@surveyMarkRead')->name('surveyMarkRead')->where('id','[0-9]+');

	Route::get('/survey-history/{id}','Auth\SurveyController@surveyHistory')->name('surveyHistory')->where('id','[0-9]+');

});
/*guest  end*/

/*auth route*/
Route::middleware(['auth'])->group(function(){

	Route::get('/home',"HomeController@index")->name('home');

	Route::get('/myprofile',"HomeController@myProfile")->name('myProfile');

	Route::post('/logout',"HomeController@logout")->name('logout');

	Route::post('/update-password',"HomeController@updatePassword")->name('updatePassword');

	/*Patient route*/
	Route::get('/create-patient',"PatientController@createPatient")->name('createPatient');

	Route::post('/post-patient',"PatientController@postPatient")->name('postPatient');

	Route::get('/manage-patients/{page}',"PatientController@managePatinets")->name('managePatinets')->where('page','[0-9]+');

	Route::post('/change-patient-status',"PatientController@changePatientStatus")->name('changePatientStatus');

	Route::get('/edit-patient/{id}',"PatientController@editPatient")->name('editPatient')->where('id','[0-9]+');

	Route::get('/patient-history/{id}',"PatientController@patientHistory")->name('patientHistory')->where('id','[0-9]+');

	Route::get('/track-patient/{id}',"PatientController@trackPatient")->name('trackPatient')->where('id','[0-9]+');
	/*Patient route end*/	

	/*Facility route*/
	Route::get('/create-facility',"FacilityController@createFacility")->name('createFacility');

	Route::post('/create-facility',"FacilityController@postFacility")->name('postFacility');
	Route::get('/facilities/{page}',"FacilityController@facilities")->name('facilities')->where('page','[0-9]+');
	/*Facility route end*/

	/*users route*/
	Route::get('/manage-users/{page}',"UserController@manageUsers")->name('manageUsers')->where('page','[0-9]+');

	Route::post('/create-user',"UserController@createUser")->name('createUser');

	Route::get('/user/{id}',"UserController@user")->name('createUser')->name('id','[0-9]+');

	Route::get('/users/{page}',"UserController@users")->name('page','[0-9]+');
	/*users route end*/

	/*survey route*/
	Route::get('/manage-survey/{page}','Auth\SurveyController@manageSurvey')->name('manageSurvey')->where('page','[0-9]+');

	Route::get('/survey-detail/{id}','Auth\SurveyController@surveyDetail')->name('surveyDetail')->where('id','[0-9]+');

	Route::get('/survey-read-list/{page}','Auth\SurveyController@surveyReadList')->name('surveyReadList')->where('id','[0-9]+');

	Route::post('/mark-as-patinet','Auth\SurveyController@markAsPatient')->name('markAsPatient');

	Route::get('/getmap-data',"Auth\SurveyController@getMapData");
	/*survey route end*/

	/*symptom route*/
	Route::get('/manage-symptoms/{page}','SymptomController@manageSymptoms')->name('manageSymptoms')->where('page','[0-9]+');

	Route::post('/create-symptoms','SymptomController@createSymptom')->name('createSymptom');

	Route::get('/delete-symptoms/{id}','SymptomController@deleteSymptom')->name('deleteSymptom')->where('id','[0-9]+');

	/*symptom route end*/

	/*authority user*/
	Route::get('/manage-authority-users/{page}',"AuthorityController@manageUsers")->name('manageAuthority')->where('page','[0-9]+');

	Route::post('/create-authority-user',"AuthorityController@createAuthority")->name('createAuthority');


	/*authority user end*/

	/*monitoring route start*/
	Route::get('/quarantine-monitoring/{page}','MonitoringController@manage')->name('manageQuarantine')->where('page','[0-9]+');
	/*monitoring route end*/

});
/*auth route end*/
