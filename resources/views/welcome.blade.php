@include('landing_header')
    <!-- banner part start-->
  
    <section class="banner_part" style="padding-top:120px;background:transparent;">
        <div class="container-fluid">
            <div class="row text-center">
                <div class="col-md-7">
                    <h2>Live updates of COVID-19</h2>
                    <div class="row" style="padding-top: 30px;">
                        <div class="col-md-8">
                            <canvas id="myChart" style="width:100%;margin:0px;padding:0px"></canvas>
                        </div>
                        <div class="col-md-4">
                            <table class="table table-bordered">
                                <tbody id="livetable" style="text-align:left;">

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 offset-md-1">
                    <h2>Corona Virus</h2>
                    <div class="row" style="padding-top: 30px">
                        <img src="{{url('public/assets/images/combine.png')}}" class="img-fluid" alt="" style="width:80%">
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- banner part start-->

    <!-- about us part start-->
    <section class="about_us padding_top">
        <div class="container">
            <div class="row justify-content-between align-items-center">
                <div class="col-md-6 col-lg-6">
                    <div class="about_us_img">
                        <img src="{{url('public/assets/images/PICTURE_1.png')}}" alt="">
                    </div>
                </div>
                <div class="col-md-6 col-lg-5">
                    <div class="about_us_text">
                        <div class="section_tittle" style="margin:0px">
                            <h2>What is Coronavirus?</h2>
                        </div>
                            
                            <p>Coronaviruses (CoV) are a large family of viruses that cause illness ranging from the common cold to more severe diseases such as Middle East Respiratory Syndrome (MERS-CoV) and Severe Acute Respiratory Syndrome (SARS-CoV).

                            Coronavirus disease (COVID-19) is a new strain that was discovered in 2019 and has not been previously identified in humans. </p>
                            <a href="{{route('surveyForm')}}" class="btn_2">Let us Know About Your Illness</a>
                        
                        <div class="banner_item">
                            <div class="single_item">
                                <img src="{{url('public/assets/img/icon/banner_1.svg')}}" alt="">
                                <h5>Emergency</h5>
                            </div>
                            <div class="single_item">
                                <img src="{{url('public/assets/img/icon/banner_2.svg')}}" alt="">
                                <h5>Appointment</h5>
                            </div>
                            <div class="single_item">
                                <img src="{{url('public/assets/img/icon/banner_3.svg')}}" alt="">
                                <h5>Qualfied</h5>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- about us part end-->

    <!-- feature_part start-->
    <section class="feature_part">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-xl-8">
                    <div class="section_tittle text-center">
                        <h2>Basic protective measures against the new Coronavirus</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="card">
                      <img src="{{url('public/assets/images/PICTURE_4.png')}}" class="card-img-top" alt="...">
                      <div class="card-body">
                        <h5 class="card-title">Wash your hands frequently and Maintain social distancin</h5>
                        <p class="card-text">
                        Wash your hands frequently and Maintain social distancingWashing your hands with soap and water or using alcohol-based hand rub kills viruses that may be on your hands.
                        </p>
                        
                      </div>
                    </div>                    
                </div>                

                <div class="col-md-4">
                    <div class="card">
                      <img src="{{url('public/assets/images/PICTURE_7.png')}}" class="card-img-top" alt="...">
                      <div class="card-body">
                        <h5 class="card-title">Practice respiratory hygiene and Avoid touching eyes, nose and mouth </h5>
                        <p class="card-text">
                        Make sure you, and the people around you, follow good respiratory hygiene. This means covering your mouth and nose with your bent elbow or tissue when you cough or sneeze. Then dispose of the used tissue immediately. 
                        </p>
                        
                      </div>
                    </div>                    
                </div>               
                <div class="col-md-4">
                    <div class="card">
                      <img src="{{url('public/assets/images/PICTURE_10.png')}}" class="card-img-top" alt="...">
                      <div class="card-body">
                        <h5 class="card-title">If you have fever, cough and difficulty breathing, seek medical care early </h5>
                        <p class="card-text">
                        Stay home if you feel unwell. If you have a fever, cough and difficulty breathing, seek medical attention and call in advance. Follow the directions of your local health authority. 
                        </p>
                        
                      </div>
                    </div>                    
                </div>
            </div>
            <!--row end-->

            <!--row start-->
            <div class="row" style="margin-top: 50px;">
                <div class="col-md-4">
                    <div class="card">
                      <img src="{{url('public/assets/images/PICTURE_5.png')}}" class="card-img-top" alt="...">
                      <div class="card-body">
                       
                        <p class="card-text">
                        Washing your hands with soap and water or using alcohol-based hand rub kills viruses that may be on your hands.
                        </p>
                        
                      </div>
                    </div>                    
                </div>                

                <div class="col-md-4">
                    <div class="card">
                      <img src="{{url('public/assets/images/PICTURE_6.png')}}" class="card-img-top" alt="...">
                      <div class="card-body">
                       
                        <p class="card-text">
                        Maintain at least 1 metre (3 feet) distance between yourself and anyone who is coughing or sneezing. If you are too close, you can affected by coughing or sneezing droplets.
                        </p>
                        
                      </div>
                    </div>                    
                </div>               
                <div class="col-md-4">
                    <div class="card">
                      <img src="{{url('public/assets/images/PICTURE_8.png')}}" class="card-img-top" alt="...">
                      <div class="card-body">

                        <p class="card-text">
                        Droplets spread virus. By following good respiratory hygiene you protect the people around you from viruses such as cold, flu and COVID-19. 
                        </p>
                        
                      </div>
                    </div>                    
                </div>
            </div>            
            <!--row start end-->
        </div>
    </section>
    <!-- feature_part start-->


    <!--::regervation_part start::-->
    <section class="regervation_part section_padding">
        <div class="container">
            <div class="row align-items-center regervation_content">
                <div class="col-lg-12">
                    <h1 class="big-title">Stay Home, Stay Safe!</h1>
                </div>

            </div>
        </div>
    </section>
    <!--::regervation_part end::-->


    <!-- footer part start-->
@include('landing_footer')

