@extends('/dashboard-layouts/master')

@section('content')
			<!-- Content area -->
			<div class="content row">

			<!-- Quick stats boxes -->
				<div class="card col-md-9" style="padding:0px">

					<div class="card-body">
						<div id="map" style="height:500px"></div>
					</div>
				</div>
				<div class="col-md-3">
					<div class="form-group">
						<select class="form-control" id="mySelect" onchange="initMap()">
							<option value="">Select District</option>
							<option value="1">Dhaka</option>
							<option value="2">Khulna</option>
							<option value="3">Chottogram</option>
						</select>						
					</div>					
						<div class="alert alert-warning">
							TOTAL
							<h2>Confirm Case: 30</h2>
							<h2>Quarantine: 200</h2>
							<h2>Dead: 3</h2>
							
						</div>
					
				</div>
			<!-- /quick stats boxes -->
			</div>
			<!-- /content area -->

@endsection

@section('js')
    <script>
    let data = [
    	{id:1,name:'Dhaka',lat:'23.810331',long:'90.412521',confirm:20,dead:2,quarantine:10},
    	{id:2,name:'Khulna',lat:'22.845640',long:'89.540329',confirm:5,dead:2,quarantine:10},
    	{id:3,name:'Chottogram',lat:'22.356852',long:'91.783180',confirm:10,dead:2,quarantine:10},
    ] ;
    function change()
    {

    }  
    // Initialize and add the map
    function initMap() {
//change marker
		let bd;
  		var x = document.getElementById("mySelect").value;

      // 23.777176, 90.399452.
//change marker position    
  		if(x>0)
  		{
	  		for(let i of data)
	  		{
	  			if(x==i.id)
	  			{
	  			  //alert(i.lat+i.long);
	  			  bd = {lat: parseFloat(i.lat), lng: parseFloat(i.long)};

		    		
	  			}
	  		}  			
  		}
  		else
  		{
  			 bd = {lat: 24, lng: 90};
  		}
//change marker end       
      
      // The map, centered at bd
      var map = new google.maps.Map(
          document.getElementById('map'), {
          	zoom: 7, 
          	center: bd,
          	mapTypeId: 'terrain',
         
          });
      // The marker, positioned at bd
 
     
      //var marker = new google.maps.Marker({position: bd, map: map});
//creating multiple marker    
    	for(let d of data)
    	{
    		//info window
    		let content = `<table class='table table-hover table-bordered'>
			      <tr>
			        <td>Location</td>
			        <td>${d.name}</td>
			      </tr>      
			      <tr>
			        <td>Confirm Case</td>
			        <td>${d.confirm}</td>
			      </tr>      
			      <tr>
			        <td>Total Death</td>
			        <td>${d.dead}</td>
			      </tr>      
			      <tr>
			        <td>Quarantine</td>
			        <td>${d.quarantine}</td>
			      </tr>
			    </table>`;	
			let infowindow = new google.maps.InfoWindow({
			    content: content
			  });

          var latLng = new google.maps.LatLng(d.lat,d.long);
          var marker = new google.maps.Marker({
            position: latLng,
            map: map,
            icon:{
		          path: google.maps.SymbolPath.CIRCLE,
		          fillColor: 'red',
		          fillOpacity: .5,
		          scale: Math.pow(2, 5) / 2,
		          strokeColor: 'white',
		          strokeWeight: .5

	      	},
	      	//label:d.name,
	      	title:d.name,
          });
          //marker event listener
			  marker.addListener('click', function() {
			    infowindow.open(map, marker);
			  });

          //listener end
    	}  
//creating multiple marker end      
    }


    </script>
    <script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyDgB7vF_nyTAIBpEyHUjtE0bzNXoTNrqcc&callback=initMap"
    async defer></script>
@endsection