@extends('/dashboard-layouts/master')

@section('content')
<div class="content">
      <!--flash message-->
      @include('elements.errors')
      <!--flash message end-->
				<!-- Basic datatable -->

				<div class="card">

					<div class="card-header header-elements-inline">
						<h5 class="card-title">
							Survey Detail
						</h5>
						<div class="header-elements">
							<div class="list-icons">
		                		<a class="list-icons-item" data-action="collapse"></a>
		                		<a class="list-icons-item" data-action="reload"></a>
		                		<a class="list-icons-item" data-action="remove"></a>
		                	</div>
	                	</div>
					</div>

        <div class="table-responsive" >
          <label class="badge">BASIC INFO</label>
					<table class="table ">
            <thead>
                <th>Date</th>
                <th>Full Name</th>
              
                <th>NID No</th>
                <th>Birth Certificate</th>
            </thead>
            <tbody>
              <tr>
                <td>
                  <?php 
                    $str = date_create($records->surveyDate);
                    echo date_format($str,'Y-m-d');
                  ?>
                </td>

                <td>{{$records->fullName}}</td>

                <td>{{$records->nidNo}}</td>
                <td>{{$records->birthCertificateNo}}</td>
              </tr>
            </tbody>
					</table>         
          <label class="badge">DETAIL INFO</label>
          <table class="table ">
            <thead>
                <th>Mobile Number</th>
                <th>Date of Birth</th>
                <th>Gender</th>
                <th>Meet with Foreigners / Suspected Person</th>
                
                <th>No. of Contact Person</th>                
                <th>Symptoms</th>
            </thead>
            <tbody>
              <tr>
                <td>
                  {{$records->mobileNumber}}
                </td>

                <td>{{$records->dateOfBirth}}</td>
                <td>{{$records->gender}}</td>
                <td>
                  <?php 
                    $foreignVisit = $records->isForeignVisitor;
                    if($foreignVisit =="true")
                    {
                      echo "<span class='badge badge-danger'>Yes</span>";
                    }
                    else
                    {
                      echo "<span class='badge badge-primary'>No</span>"; 
                    }
                  ?>
                </td>
                
                <td>{{$records->numOfContInftPerson}}</td>
                <td>
                  <?php 
                  $s = explode(',', $records->physicalSymptoms);
                  foreach ($s as $key => $value) {
                    if(!empty($value))
                    {
                      echo "<span class='badge badge-danger'> $value </span>";
                    }
                    
                  }
                  ?>
                </td>
                
              </tr>
            </tbody>
          </table>
          <label class="badge">REMARKS</label>
          <p style="margin:10px">{{$records->remarks}}</p>
          <label class="badge">PRESENT ADDRESS</label>
          <table class="table ">
            <thead>
                <th>Village</th>
                <th>Post Office</th>
                <th>Post Code</th>
                <th>Upazila</th>
                <th>District</th>
                <th>Division</th>                

            </thead>
            <tbody>
              <tr>
                <td>
                  {{$records->presentAddress->village}}
                </td>

                <td>{{$records->presentAddress->postOffice}}</td>
                <td>{{$records->presentAddress->postCode}}</td>
                <td>{{$records->presentAddress->upazila}}</td>
                <td>{{$records->presentAddress->district}}</td>
                <td>{{$records->presentAddress->division}}</td>
                
              </tr>
            </tbody>
          </table>          
            
<!--pag-->
         
<!--pag-->        
        </div>  
				</div>
				<!-- /basic datatable -->		

</div>


<!--add new client modal-->
<!-- Modal -->
<div class="modal fade" id="updateFacility" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-scrollable" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Update Facility</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
                    <form class="form" method="post" action="{{route('postFacility')}}">
                      @csrf
                      <input type="hidden" name="id" value="">
                        <div class="form-group row">
                            <div class="col-md-4">
                                <label>Facility Name*</label>
                                <input type="" name="facilityName" class="form-control" required>
                            </div>                            
                            <div class="col-md-4">
                                <label>Number of Bed*</label>
                                <input type="number" name="numberOfBeds" class="form-control" required>
                            </div>                            
                            <div class="col-md-4">
                                <label>Number of Doctor*</label>
                                <input type="number" name="numberOfDoctors" class="form-control">
                            </div>
                        </div>                          

                        <div class="form-group row">
                            <div class="col-md-4">
                                <label>Number of ICU*</label>
                                <input type="number" name="numOfIcu" class="form-control" required>
                            </div>                             
                            <div class="col-md-4">
                                <label>Number of Volunteer*</label>
                                <input type="number" name="numberOfVolunteers" class="form-control">
                            </div>                            
                            <div class="col-md-4">
                                <label>Help Line</label>
                                <input type="number" name="helpLineNumber" class="form-control">
                            </div>                            

                        </div>                        

                        <label class="label"><strong>PRESENT ADDRESS</strong></label>
                        <div class="form-group row">
                            <div class="col-md-4">
                                <label>Village*</label>
                                <input type="" name="village" class="form-control" required>
                            </div>                            
                            <div class="col-md-4 no-p">
                                <label>Post Office*</label>
                                <input type="" name="postOffice" class="form-control" required>
                            </div>                              
                            <div class="col-md-4 no-p">
                                <label>Post Code*</label>
                                <input type="" name="postCode" class="form-control"required>
                            </div>                             
                            <div class="col-md-4">
                                <label>Upazilla*</label>
                                <input type="" name="upazila" class="form-control"required>
                            </div>                            
                            <div class="col-md-4 no-p">
                                <label>District*</label>
                                <input type="" name="district" class="form-control" required>
                            </div>                              
                            <div class="col-md-4 no-p">
                                <label>Division*</label>
                                <input type="" name="division" class="form-control" required>
                            </div>                            

                        </div>   


                        
                        <div class="form-group row">
                            <div class="col-md-12">
                                <button class="btn btn-primary btn-lg">Submit Information</button>
                            </div>
                        </div>
                    </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        
      </div>
    </div>
  </div>
</div>
<!--add new client modal end-->
<!--spinner overlay-->
@include('elements.spinner')
<!--spinner overlay end-->

@endsection

@section('js')
<script type="text/javascript">

</script>


@endsection
