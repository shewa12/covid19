@extends('/dashboard-layouts/master')

@section('content')
<div class="content">
      <!--flash message-->
    @include('elements.errors')
      <!--flash message end-->
	<div class="row">
      <div class="col-md-4">
          <div class="card">
              
              <div class="card-body">

                @if(empty(Auth::user()->image))
                  <img src="{{url('public/images/avatar.png')}}" class="rounded-circle avatar">
                @else
                  <img src="{{url('storage/app/avatars')}}/{{Auth::user()->image}}"class="rounded-circle avatar"> 
                @endif  
                <form  action="" method="post" enctype="multipart/form-data">
                  @csrf
                    <div class="form-group">
                      <label>Upload Photo </label>
                      <input class="form-control" type="file" class="form-control" name="image" required/>
                    </div>
                    <div class="form-group">
                      <button class="btn-primary btn btn-block">
                          Update Photo
                      </button>
                    </div>
                </form>                

              </div>

          </div>

      </div>
      <div class="col-md-6">
          <div class="card">
            <div class="card-body">
              <form action="{{route('updatePassword')}}" method="post">
                @csrf
              

                <div class="form-group">
                  <label>New Password</label>
                  <input  class="form-control" type="password" name="password" placeholder="new password">
                </div>    

                <div class="form-group">
                  <label>Confirm Password</label>
                  <input class="form-control" type="password" name="password_confirmation" placeholder="confirm password">
                </div>                

                <div class="form-group">
                    <button class="btn-primary btn btn-block">Update Password</button>
                </div>

              </form>
            </div>
          </div>
      </div>
  </div>

</div>

@endsection

@section('js')
<script src="{{url('public/assets/js/app.js')}}"></script>
@endsection
