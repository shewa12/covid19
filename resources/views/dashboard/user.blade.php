@extends('/dashboard-layouts/master')

@section('content')
<div class="content">
      <!--flash message-->
      @include('elements.errors')
      <!--flash message end-->
				<!-- Basic datatable -->

				<div class="card">

					<div class="card-header header-elements-inline">
						<h5 class="card-title">
							<a href="#"  onClick="resetForm('addForm')" data-toggle="modal" data-target="#addUser" class="btn-primary btn"><i class="fas fa-plus-circle"></i> Add User</a>
						</h5>
						<div class="header-elements">
							<div class="list-icons">
		                		<a class="list-icons-item" data-action="collapse"></a>
		                		<a class="list-icons-item" data-action="reload"></a>
		                		<a class="list-icons-item" data-action="remove"></a>
		                	</div>
	                	</div>
					</div>

        <div class="table-responsive" >

					<table class="table datatable-basic">
						<thead>
							<tr>
                <th>Sl No.</th>
								<th>Name</th>
                <th>Email</th>
                <th>Facility Name</th>                
								<th>Status</th>
								<th class="text-center">Actions</th>
							</tr>
						</thead>
						<tbody>
              <?php $i=1;?>
              @forelse($records as $r)
              <?php 
                $id= $r->id;
                $username= $r->username;
                $email= $r->email;
                $activated= $r->activated;                
                $facilityId= $r->facilityId;
                $facilityName= $r->facilityName;

              ?>  
							<tr>
                <td>{{$i++}}</td>
								<td>{{$username}}</td>
								<td>{{$email}}</td>
                <td>{{$facilityName}}</td>
								<td>
                  <?php 
                  if($activated===true)
                  {
                    echo '<label class="badge badge-primary">Active </label>';
                  }
                  else
                  {
                    echo '<label class="badge badge-danger">Deactive</label>';
                  }
                  ?>
      
                </td>
								

								<td class="text-center">
									<div class="list-icons">
										<div class="dropdown">
											<a href="#" class="list-icons-item" data-toggle="dropdown">
												<i class="icon-menu9"></i>
											</a>

											<div class="dropdown-menu dropdown-menu-right">
                        <?php 

                        echo '
                          <a href="#" onClick="edit('.$id.',\''.$username.'\' ,\''.$email.'\',\''.$activated.'\',\''.$facilityId.'\',\''.$facilityName.'\')" data-toggle="modal" data-target="#updateUser" class="dropdown-item"><i class="fas fa-edit"></i> Edit</a>
                        ';
                        ?>

                    
                   
                      <!--  <button type="submit" class="dropdown-item delete"id="{{$r->id}}"><i class="far fa-trash-alt" ></i> Delete 
                        </button>
											-->
											</div>
										</div>
									</div>
								</td>
							</tr>
              @empty
              <tr>
                <td>No record found</td>
              </tr>
              @endforelse
						</tbody>
					</table>
<!--pagination-->    
          @if(count($records)>100)
          <nav aria-label="Page navigation example">
            <ul class="pagination" style="padding:10px">
              <li class="page-item">
                @if($prev <= 0) 
                <a class="page-link" disabled="">Previous</a> 
                @else 
                <a class="page-link" href="{{url('manage-users')}}/{{$prev}}" disabled="">Previous</a>
                @endif
                
              </li>

              <li class="page-item"><a class="page-link" href="{{url('manage-users')}}/{{$next}}">Next</a></li>
            </ul>
          </nav> 
          @endif       
<!--pagination end-->          
        </div>  
				</div>
				<!-- /basic datatable -->		

</div>
<!--add new client modal-->
<!-- Modal -->
<div class="modal fade" id="addUser" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-scrollable" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Add User</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      		<form class="form-horizontal" method="post" action="{{route('createUser')}}" id="addForm">
            @csrf
            
      			<div class="form-group">
      				<label>Select Facility</label>
      				<select class="form-control" name="facilityId" required>
                <option>Select</option>
                @forelse($facilities as $f)
                <option value="{{$f->id}}">{{$f->facilityName}}</option>
                @empty
                <option>No record found</option>
                @endforelse

              </select>
      			</div>             

            <div class="form-group">
              <label>User Name</label>
              <input class="form-control" name="username" required>
            </div> 

            <div class="form-group">
              <label>Email</label>
              <input type="email" class="form-control" name="email" required>
            </div>

            <div class="form-group">
              <label>Password</label>
              <input type="password" class="form-control" name="password" minlength="6">
            </div>            

            <div class="form-group">
              <label>Status</label>
              <select class="form-control" name="activated">
                  <option value="true">Active</option>
                  <option value="false">Deactive</option>
              </select>
            </div>


      			<div class="form-group" id="addingResponse">
              
            </div>     			
      			<div class="form-group">
      				
      				<button class="btn-primary btn">Add User</button>
      			</div>
      			

      		</form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        
      </div>
    </div>
  </div>
</div>
<!--add new client modal end-->

<!--add new client modal-->
<!-- Modal -->
<div class="modal fade" id="updateUser" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-scrollable" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Update User</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      		<form class="form-horizontal" method="post" action="{{route('createUser')}}" id="updateForm">
            @csrf
            <input type="hidden" name="id">
            <div class="form-group">
              <label>Select Facility</label>
              <select class="form-control" id="facilityId"  name="facilityId" required>
                @forelse($facilities as $f)
                <option value="{{$f->id}}">{{$f->facilityName}}</option>
                @empty
                <option>No record found</option>
                @endforelse

              </select>
            </div>             

            <div class="form-group">
              <label>User Name</label>
              <input class="form-control" name="username" required>
            </div> 

            <div class="form-group">
              <label>Email</label>
              <input type="email" class="form-control" name="email" required>
            </div>

        
            <div class="form-group">
              <label>Status</label>
              <select class="form-control" name="activated" id="activated">
                <option value="true">Activated</option>
                <option value="false">Deactivated</option>
              </select>
            </div>
            <div class="form-group" id="updatingResponse">
              
            </div>
      			      			
      			<div class="form-group">
      				<button class="btn-primary btn">Update User</button>
      			</div>
      			

      		</form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        
      </div>
    </div>
  </div>
</div>
<!--add new client modal end-->
<!--spinner overlay-->
@include('elements.spinner')
<!--spinner overlay end-->

@endsection

@section('js')
<script type="text/javascript">

  function edit(id,name,email,activated,facilityId,facilityName){
 
      $('[name="id"]').val(id);
      $('[name="username"]').val(name);
      $('[name="email"]').val(email);
      
      $('#facilityId').prepend(`<option value=${facilityId} selected>${facilityName}</option>`);

      if(activated==1)
      {
        $("#activated").prepend(`<option value='true'>Activate</option>`);
      }
      else
      {
        $("#activated").prepend(`<option value='false'>Deactivate</option>`);
      }
  }
  //reset addForm
  function resetForm(formId)
  {
    document.getElementById(formId).reset();
  }
</script>


@endsection
