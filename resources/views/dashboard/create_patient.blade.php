@extends('/dashboard-layouts/master')

@section('content')
<div class="content">
      <!--flash message-->
      @include('elements.errors')
      <!--flash message end-->
				<!-- Basic datatable -->

				<div class="card">

					<div class="card-header header-elements-inline">
						<h5 class="card-title">
							Create Patient					
                        </h5>
						<div class="header-elements">
							<div class="list-icons">
		                		<a class="list-icons-item" data-action="collapse"></a>
		                		<a class="list-icons-item" data-action="reload"></a>
		                		<a class="list-icons-item" data-action="remove"></a>
		                	</div>
	                	</div>
					</div>

          <div class="card-body">
                  <span style="color:red">* marked fields are required</span> 
                    <form class="form" method="post" action="{{route('postPatient')}}">
                        @csrf
                        <div class="form-group row">
                            <div class="col-md-4">
                                <label>Full Name*</label>
                                <input type="" name="fullName" class="form-control" required value="{{ old('fullName') }}">
                            </div>                            
                            <div class="col-md-4">
                                <label>Father's Name*</label>
                                <input type="" name="fatherName" class="form-control"  required value="{{old('fatherName')}}">
                            </div>                            
                            <div class="col-md-4">
                                <label>Mother's Name*</label>
                                <input type="" name="motherName" class="form-control"  required value="{{old('motherName')}}">
                            </div>  
                        </div>                        

                        <label class="label"><strong>NID OR BIRTH CERTIFICATE HAVE TO FILL</strong></label>
                        <div class="form-group row">
                          
                            <div class="col-md-6">
                                <label>National ID No:</label>
                                <input type="number" name="nidNo" class="form-control" value="{{old('nidNo')}}">
                            </div>                            
                            <div class="col-md-6 no-p">
                                <label>Birth Certificate No:</label>
                                <input type="number" name="birthCertificateNo" class="form-control" value="{{old('birthCertificateNo')}}">
                            </div>                            

                        </div>                          
                        <label class="label"><strong>PRESENT ADDRESS</strong></label>
                        <div class="form-group row">
                            <div class="col-md-4">
                                <label>Village*</label>
                                <input type="" name="village" class="form-control" required value="{{old('village')}}">
                            </div>                            
                            <div class="col-md-4 no-p">
                                <label>Post Office*</label>
                                <input type="" name="postOffice" class="form-control"  required value="{{old('postOffice')}}" required>
                            </div>                              
                            <div class="col-md-4 no-p">
                                <label>Post Code</label>
                                <input type="number" name="postCode" class="form-control"  value="{{old('postCode')}}">
                            </div>                             
                            <div class="col-md-4">
                                <label>Upazila*</label>
                                <input type="" name="upazila" class="form-control"  required value="{{old('upazila')}}">
                            </div>                            
                            <div class="col-md-4 no-p">
                                <label>District*</label>
                                <input type="" name="district" class="form-control"  required value="{{old('district')}}">
                            </div>                              
                            <div class="col-md-4 no-p">
                                <label>Division*</label>
                                <select class="form-control" name="division" required="">
                                    <option>Select</option>
                                    @forelse($divisions as $d)
                                    @if(old('division')==$d->name)
                                    <option value="{{$d->name}}" selected="">
                                      {{$d->name}}</option>
                                    @else
                                    <option value="{{$d->name}}" >
                                      {{$d->name}}</option>
                                      @endif  
                                    @empty
                                    <option>No record found</option>
                                    @endforelse
                                </select>
                            </div>                            

                        </div>   
                        <input type="checkbox" id="same" name="same">
                        <label for="same">Same as Present Address</label>
                        <br>
                        <label class="label">
                            <strong>PERMANENT ADDRESS</strong>
                        </label>
                        <div class="form-group row">
                            <div class="col-md-4">
                                <label>Village*</label>
                                <input type="" id="village" name="per_village" class="form-control" required value="{{old('village')}}">
                            </div>                            
                            <div class="col-md-4 no-p">
                                <label>Post Office*</label>
                                <input type="" name="per_postOffice" id="postOffice" class="form-control" value="{{old('postOffice')}}" required>
                            </div>                              
                            <div class="col-md-4 no-p">
                                <label>Post Code</label>
                                <input type="number" name="per_postCode" id="postCode" class="form-control"  value="{{old('postCode')}}">
                            </div>                             
                            <div class="col-md-4">
                                <label>Upazila*</label>
                                <input type="" name="per_upazila" class="form-control" id="upazila"  required value="{{old('upazila')}}">
                            </div>                            
                            <div class="col-md-4 no-p">
                                <label>District*</label>
                                <input type="" id="district" name="per_district" class="form-control"  required value="{{old('district')}}">
                            </div>                              
                            <div class="col-md-4 no-p">
                                <label>Division*</label>
                                <select class="form-control" name="per_division" id="division" required="">
                                    <option>Select</option>
                                    @forelse($divisions as $d)
                                    @if(old('division')==$d->name)
                                    <option value="{{$d->name}}" selected="">
                                      {{$d->name}}</option>
                                    @else
                                    <option value="{{$d->name}}" >
                                      {{$d->name}}</option>
                                      @endif  
                                    @empty
                                    <option>No record found</option>
                                    @endforelse
                                </select>
                            </div>  
                           
                        </div>   

                        <div class="form-group row">
                            <div class="col-md-6">
                                <label>Mobile No:*</label>
                                <input type="number" name="mobileNumber" class="form-control" minlength="11" required value="{{old('mobileNumber')}}">
                            </div>                            
                            <div class="col-md-6 no-p">
                                <label>Date of Birth* (1990-01-01 mm-dd-yyyy)</label>
                                <input type="date" max="<?= date('Y-m-d',strtotime("-1 days"));?>" name="dateOfBirth" class="form-control" required value="{{old('dateOfBirth')}}">
                            </div>                            

                        </div>                         

                        <div class="form-group row">
                            <div class="col-md-4">
                                <label>Gender*</label>
                                <select class="form-control" name="gender" required="">
                                  @if(!empty(old('gender')))
                                  <option value="{{old('gender')}}">{{old('gender')}}</option>
                                  @else
                                  <option value="">Select</option>
                                  @endif
                                    
                                    <option value="Male">Male</option>
                                    <option value="Female">Female</option>
                                    <option value="Other">Other</option>
                                </select>
                            </div>                            
                            <div class="col-md-4">
                                <label>Blood Group*</label>
                                <select class="form-control" name="bloodGroup" required>
                                    <option value="A+">A+</option>
                                    <option value="A-">A-</option>
                                    <option value="B+">B+</option>
                                    <option value="B-">B-</option>
                                    <option value="AB+">AB+</option>
                                    <option value="AB-">AB-</option>
                                    <option value="O+">O+</option>
                                    <option value="O-">O-</option>
                                </select>                            
                              </div>                              
                            <div class="col-md-4">
                                <label>Number of Days Sick*</label>
                                <input type="number" name="numOfDaysSick" class="form-control"  required value="{{old('numOfDaysSick')}}">
                            </div>                            

                        </div> 

                        <div class="form-group row">
                            <div class="col-md-4">
                                <label>Status*</label>
                                <select class="form-control" name="status" required> 
                                  @if(!empty(old('status')))
                                  <option value="{{old('status')}}">
                                    {{old('status')}}
                                  </option>
                                  @else
                                  <option value="">Select
                                  </option>
                                  @endif

                                  <option value="home_quarantine">Home Quarantine</option>
                                  <option value="quarantine">Quarantine</option>
                                  <option value="isolation">Isolation</option>
                                  <option value="affected">Affected</option>
                                  <option value="recovered">Recovered</option>
                                  <option value="dead">Dead</option>
                                  <option value="released">Released</option>
                                </select>
                            </div>                               
                        </div>  
                                              
                        <div class="form-group row">                               
                            <div class="col-md-12">
                                <label>Physical Symptoms* (at least one symptom)</label>
                                <br>

                                @forelse($symptoms as $s)
                                    <div class="form-check form-check-inline col-md-10">
                                      <input class="form-check-input" type="checkbox" id="{{$s->name}}" value="{{$s->id}}" name="physicalSymptoms[]">
                                      <label class="form-check-label col-md-4" for="{{$s->name}}">{{$s->name}}</label>

                                      <input type="number" name="days[]" class="form-control col-md-4" min="0" placeholder="Number of days">
                                    </div>                                
                                @empty
                                
                                @endforelse
                            </div>   
                        </div>    

                        <div class="form-group row">
                            <div class="col-md-4">
                                <label>Meet with Foreigners / Suspected Person*</label>
                                <div class="form-check">
                                  <input class="form-check-input" type="radio" name="isForeignVisitor" id="yes" value="true" selected required>
                                  <label class="form-check-label" for="yes">
                                    Yes
                                  </label>
                                </div>                               
                                 <div class="form-check">
                                  <input class="form-check-input" type="radio" name="isForeignVisitor" id="no" value="false" required>
                                  <label class="form-check-label" for="no">
                                    No
                                  </label>
                                </div>
                            </div>   
                        </div> 
                        <div class="form-group row">
                            <div class="col-md-6">
                                <label>Remarks</label>
                                <textarea class="form-control" name="remarks">{{old('remarks')}}</textarea>
                            </div>  
                            <div class="col-md-6">
                                <label>Recommendation</label>
                                <textarea class="form-control" name="recommendation">{{old('recommendation')}}</textarea>
                            </div>                              
                        </div>                        
                       

                        
                        <div class="form-group row">
                            <div class="col-md-12">
                                <button class="btn btn-primary btn-lg">Submit Information</button>
                            </div>
                        </div>
                    </form>
          </div>  
				</div>
				<!-- /basic datatable -->		

</div>
<!--add new client modal-->
<!-- Modal -->
<div class="modal fade" id="addClient" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-scrollable" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Add New Client</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      		<form class="form-horizontal" method="post" action="" id="addForm">
            @csrf
      			<div class="form-group">
      				<label>Name</label>
      				<input class="form-control" name="name" required>
      			</div> 

            <div class="form-group">
              <label>Email</label>
              <input type="email" class="form-control" name="email" required>
            </div>

            <div class="form-group">
              <label>Phone</label>
              <input type="number" class="form-control" name="phone">
            </div>

      			<div class="form-group">
      				<label>Address</label>
      				<input class="form-control" name="address">
      			</div> 
      			     			
      			<div class="form-group">
      				<label>City</label>
      				<input class="form-control" name="city">
      			</div>      			

      			<div class="form-group">
      				<label>ZIP</label>
      				<input class="form-control" name="zip">
      			</div>

      			<div class="form-group">
      				<label>Notes</label>
      				<textarea class="form-control" name="notes"></textarea>
      			</div>
      			<div class="form-group" id="addingResponse">
              
            </div>     			
      			<div class="form-group">
      				
      				<button class="btn-primary btn">Add Client</button>
      			</div>
      			

      		</form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        
      </div>
    </div>
  </div>
</div>
<!--add new client modal end-->

<!--add new client modal-->
<!-- Modal -->
<div class="modal fade" id="editClient" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-scrollable" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Edit Client</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      		<form class="form-horizontal" method="post" action="" id="updateForm">
            @csrf
            <input type="hidden" name="id">
      			<div class="form-group">
      				<label>Name</label>
      				<input class="form-control" name="name">
      			</div>

            <div class="form-group">
              <label>Phone</label>
              <input type="number" class="form-control" name="phone">
            </div>

            <div class="form-group">
              <label>Email</label>
              <input type="email" class="form-control" name="email" required>
            </div>

      			<div class="form-group">
      				<label>Address</label>
      				<input class="form-control" name="address">
      			</div> 
      			     			
      			<div class="form-group">
      				<label>City</label>
      				<input class="form-control" name="city">
      			</div>      			

      			<div class="form-group">
      				<label>ZIP</label>
      				<input class="form-control" name="zip">
      			</div>

      			<div class="form-group">
      				<label>Notes</label>
      				<textarea class="form-control" name="notes"></textarea>
      			</div>           

            <div class="form-group" id="updatingResponse">
              
            </div>
      			      			
      			<div class="form-group">
      				<button class="btn-primary btn">Update Client</button>
      			</div>
      			

      		</form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        
      </div>
    </div>
  </div>
</div>
<!--add new client modal end-->
<!--spinner overlay-->
@include('elements.spinner')
<!--spinner overlay end-->

@endsection

@section('js')

<script type="text/javascript">
     $(document).ready(function(){

        $('input[name="same"]').click(function(){

            if($(this).is(":checked")){
                let village = $('[name="village"]').val();
                let dis = $('[name="district"]').val();
                let div = $('[name="division"]').val();
                let upazila = $('[name="upazila"]').val();
                let poffice = $('[name="postOffice"]').val();
                let pocode = $('[name="postCode"]').val();                

                $('#village').val(village);
                $('#district').val(dis);
                $('#division').val(div);
                $('#upazila').val(upazila);
                $('#postOffice').val(poffice);
                $('#postCode').val(pocode);
            }

            else if($(this).is(":not(:checked)")){
                $('#village').val('');
                $('#district').val('');
                $('#division').val('');
                $('#upazila').val('');
                $('#postOffice').val('');
                $('#postCode').val('');

            }

        });

        //district as per div

    });
 
</script>
@endsection
