@extends('/dashboard-layouts/master')

@section('content')
			<!-- Content area -->
			<div class="content row">

			<!-- Quick stats boxes -->
				<div class="card col-md-9" style="padding:0px">

					<div class="card-body">
						<div id="map" style="height:500px"></div>
					</div>
				</div>
				<div class="col-md-3">
					<div class="form-group">
						<select class="form-control" id="mySelect" onchange="initMap()">
							
							
						</select>						
					</div>					
						<div class="alert alert-warning">
							TOTAL
							<?php 
							$status = ['home_quarantine','quarantine','isolation','affected','recovered','dead','released'];
							$keys = array_keys($body);
							

							for($i=0; $i<7;$i++)
							{
								$s = $status[$i];//each status
								for($j=0;$j<count($keys);$j++)
								{
									if($s==$keys[$j])
									{
										$str = str_replace("_", " ", $s);
										echo "<h4 class='text-uppercase'>$str : $body[$s]
										</h4>" ;
										break;
									}
								}

							}

							?>

						</div>
					
				</div>
			<!-- /quick stats boxes -->
			</div>
			<div class="content row">
					<div class="card col-md-9">
						<div class="card-body">
							<canvas id="myChart" style="width:100%;margin:0px;padding:0px"></canvas>
						</div>
					</div>
				
			</div>
			<!-- /content area -->

@endsection

@section('js')
    <script>
    	let data;
    	const url = "<?php echo url('/getmap-data')?>";
    	let res = fetch(url).then(res=>res.json());
    	res.then((res)=> {
    		if(res.status=='ok')
    		{
	    		data = JSON.parse(res.msg);
	    		
	    		for(let h of data)
	    		{
	    		 		
	    			if(h.district !=undefined && h.id !=undefined )
	    			{
	    				if(h.district=='Dhaka')
	    				{
	    					$("#mySelect").append(`<option value='${h.id}' selected>${h.district}</option>`);
	    				}
	    				else
	    				{
	    					$("#mySelect").append(`<option value='${h.id}'>${h.district}</option>`);
	    				}
	    				
	    			} 	
	    		}

	    		initMap();
    		}

    	}).catch(error=> console.log(`error: ${error}`));

    // Initialize and add the map
    function initMap() {
//change marker
		let bd;
  		var x = document.getElementById("mySelect").value;

      // 23.777176, 90.399452.
//change marker position    
  		if(x>0)
  		{
	  		for(let i of data)
	  		{
	  			if(x==i.id)
	  			{
	  			  
	  			 	bd = {lat: parseFloat(i.latitude), lng: parseFloat(i.longitude)};
		    		
	  			}
	  		}  			
  		}
  		else
  		{
  			 bd = {lat: 23.684994, lng: 90.356331};
  		}
//change marker end       
      
      // The map, centered at bd
      var map = new google.maps.Map(
          document.getElementById('map'), {
          	zoom: 8, 
          	center: bd,
          	mapTypeId: 'terrain',
         
          });
      // The marker, positioned at bd
 
     
      //var marker = new google.maps.Marker({position: bd, map: map});
//creating multiple marker 
if(data)
{
	
    	for(let d of data)
    	{
    		//info window
    		let cases = d.cases;
 			
    		let content = `<table class='table table-hover table-bordered' id="test">
			      <tr>
			        <td>Location</td>
			        <td>${d.district}</td>
			      </tr> 
			      <tr>
			        <td>Recovered</td>
			        <td>${ typeof cases.recovered !=="undefined"? cases.recovered : 0}</td>
			      </tr> 
			      <tr>
			        <td>Isolation</td>
			        <td>${ typeof cases.isolation !=="undefined"? cases.isolation :0}</td>
			      </tr> 
			      <tr>
			        <td>Home Quarantine</td>
			        <td>${typeof cases.home_quarantine !=="undefined"? cases.home_quarantine :0}</td>
			      </tr> 			      
			      <tr>
			        <td>Quarantine</td>
			        <td>${typeof cases.quarantine !=="undefined"? cases.quarantine :0}</td>
			      </tr> 			      
			      <tr>
			        <td>Dead</td>
			        <td>${typeof cases.dead !=="undefined"? cases.dead :0}</td>
			      </tr>     
			    </table>`;	
			let infowindow = new google.maps.InfoWindow({
			    content: content
			  });

          var latLng = new google.maps.LatLng(d.latitude,d.longitude);

          var marker = new google.maps.Marker({
            position: latLng,
            map: map,
            icon:{
		          path: google.maps.SymbolPath.CIRCLE,
		          fillColor: 'red',
		          fillOpacity: .5,
		          scale: Math.pow(2, 5) / 2,
		          strokeColor: 'white',
		          strokeWeight: .5

	      	},
	      	//label:d.name,
	      	title:d.name,
          });
          //marker event listener
			marker.addListener('click', function() {
			    infowindow.open(map, this);
			});

          //listener end
    	}  		
}
//creating multiple marker end      
    }


    </script>
<!--pie chart -->   
    
    <!-- popper js -->
    <script src="{{url('public/assets/js/popper.min.js')}}"></script>
    <!-- bootstrap js -->
    <script src="{{url('public/assets/js/bootstrap.min.js')}}"></script>
    <script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0"></script>
<script type="text/javascript">
var ctx = document.getElementById('myChart').getContext('2d');
const urlSummerize = "<?php echo url('/get-summerize')?>";

let response = fetch(urlSummerize);
response.then(res => res.json()).then((res)=> {
    if(res.status=='ok')
    {
        let data = res.msg;
//pie start    
        var myPieChart = new Chart(ctx, {
            type: 'pie',
            data:{
                datasets: [{
                    data: [data.home_quarantine, data.quarantine, data.isolation,data.affected,data.recovered,data.dead],
                    backgroundColor: [
                        "#778899",
                        "#FFFF01",
                        "#F8CE48",
                        "#EB702D",
                        "#34A853",
                        "#EA4335",

                  ],            
                }],

                // These labels appear in the legend and in the tooltips when hovering different arcs
                labels: [
                    'Home Quarantine '+( typeof data.home_quarantine  !== "undefined" ? data.home_quarantine :""),
                    'Quarantine '+( typeof data.quarantine  !== "undefined" ? data.quarantine :""),
                    'Isolation '+( typeof data.isolation  !== "undefined" ? data.isolation :""),
                    'Affected '+( typeof data.affected  !== "undefined" ? data.affected :""),
                    'Recovered '+ ( typeof data.recovered  !== "undefined" ? data.recovered :""),
                    'Dead '+( typeof data.dead  !== "undefined" ? data.dead :""),
                ],

            },
            
            options: {
                legend: {
                    display: true,
                    labels: {
                       
                        fontSize: 16,
                    }
                },
                tooltips: {
                    mode: 'label',
                    callbacks: {
                        label: function(tooltipItem, data) { 
                            var indice = tooltipItem.index;                 
                            return  data.labels[indice] ;
                        }
                    }
                },
            }
        });    
//pie start end        
    }
})
.catch(error=> console.log(`Error: ${error}`));


</script>
<!--pie chart end-->    
    <script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyDgB7vF_nyTAIBpEyHUjtE0bzNXoTNrqcc&callback=initMap"
    async defer></script>

@endsection