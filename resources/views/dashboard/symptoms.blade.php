@extends('/dashboard-layouts/master')

@section('content')
<div class="content">
      <!--flash message-->
      @include('elements.errors')
      <!--flash message end-->
				<!-- Basic datatable -->

				<div class="card">

					<div class="card-header header-elements-inline">
						<h5 class="card-title">
							<a href="#"  onClick="resetForm('addForm')" data-toggle="modal" data-target="#addUser" class="btn-primary btn"><i class="fas fa-plus-circle"></i> Add Symptom</a>
						</h5>
						<div class="header-elements">
							<div class="list-icons">
		                		<a class="list-icons-item" data-action="collapse"></a>
		                		<a class="list-icons-item" data-action="reload"></a>
		                		<a class="list-icons-item" data-action="remove"></a>
		                	</div>
	                	</div>
					</div>

        <div class="table-responsive" >
          <table class="table datatable-basic">
            <thead>
              <tr>
                <th>Sl No.</th>
                <th>Name</th>

                <th class="text-center">Actions</th>
              </tr>
            </thead>
            <tbody>
              <?php $i=1;?>
              @forelse($records as $r)
              <?php 
                $id= $r->id;
                $name = $r->name;

              ?>  
              <tr>
                <td>{{$i++}}</td>
                <td>{{$name}}</td>
                <td class="text-center">
                  <div class="list-icons">
                    <div class="dropdown">
                      <a href="#" class="list-icons-item" data-toggle="dropdown">
                        <i class="icon-menu9"></i>
                      </a>

                      <div class="dropdown-menu dropdown-menu-right">

                          <a href="#" onClick="edit('<?php echo $id?>','<?php echo $name?>')" data-toggle="modal" data-target="#updateSymptom" class="dropdown-item"><i class="fas fa-edit"></i> Edit</a>          
          
                      <a href="{{route('deleteSymptom',$r->id)}}" class="dropdown-item delete"id="{{$r->id}}"><i class="far fa-trash-alt" ></i> Delete 
                        </a>
                     
                      </div>
                    </div>
                  </div>
                </td>
              </tr>
              @empty
              <tr>
                <td>No record found</td>
              </tr>
              @endforelse
            </tbody>
          </table>
       
        </div>  
				</div>
				<!-- /basic datatable -->		

</div>
<!--add new client modal-->
<!-- Modal -->
<div class="modal fade" id="addUser" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-scrollable" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Add Symptom</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      		<form class="form-horizontal" method="post" action="{{route('createSymptom')}}" id="addForm">
            @csrf
            <div class="form-group">
              <label>Name*</label>
              <input type="" name="name" class="form-control" required>
            </div>
      			<div class="form-group">
      				
      				<button class="btn-primary btn">Add Symptom</button>
      			</div>
      			
      		</form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        
      </div>
    </div>
  </div>
</div>
<!--add new client modal end-->

<!--add new client modal-->
<!-- Modal -->
<div class="modal fade" id="updateSymptom" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-scrollable" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Update Symptom</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      		<form class="form-horizontal" method="post" action="{{route('createSymptom')}}" id="updateForm">
            @csrf
            <input type="hidden" name="id">
            <div class="form-group">
              <label>Name*</label>
              <input type="" name="name" class="form-control" required="">
            </div>	
      			<div class="form-group">
      				<button class="btn-primary btn">Update Symptom</button>
      			</div>
      			
      		</form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        
      </div>
    </div>
  </div>
</div>
<!--add new client modal end-->
<!--spinner overlay-->
@include('elements.spinner')
<!--spinner overlay end-->

@endsection

@section('js')
<script type="text/javascript">

  function edit(id,name){
 
      $('[name="id"]').val(id);
      $('[name="name"]').val(name);

  }
  //reset addForm
  function resetForm(formId)
  {
    document.getElementById(formId).reset();
  }

$(document).ready(function(){
  $(".delete").on('click',function(e){
      e.preventDefault();
      let url = $(this).attr('href');
      
    //confirm    
    $.confirm({
        title: 'Do you want to delete?',
        buttons: {
            confirm: function () {
              window.location = url ;
            },
            cancel: function () {
                
            },
        }
    });      
    //confirm end      
  })
})
</script>


@endsection
