@extends('/dashboard-layouts/master')

@section('content')
<div class="content">
      <!--flash message-->
      @include('elements.errors')
      <!--flash message end-->
        <!-- Basic datatable -->

        <div class="card">

          <div class="card-header header-elements-inline">
            <h5 class="card-title">
              Manage Survey
            </h5>
            <div class="header-elements">
              <div class="list-icons">
                        <a class="list-icons-item" data-action="collapse"></a>
                        <a class="list-icons-item" data-action="reload"></a>
                        <a class="list-icons-item" data-action="remove"></a>
                      </div>
                    </div>
          </div>

        <div class="table-responsive" >
          <table class="table datatable-basic">
            <thead>
              <tr>
                <th>Sl No.</th>
                <th>Date</th>
                <th>Full Name</th>
                <th>Mobile No.</th>
                <th>District</th>  
                <th>Meet with Foreigners / Suspected Person</th>                 

                <th>Symptoms</th>
                <th>Priority</th>
                <th class="text-center">Actions</th>
              </tr>
            </thead>
            <tbody>
              <?php $i=1;?>
              @forelse($records as $r)
 
              <tr>
                <td>{{$i++}}</td>
                <td>{{$r->surveyDate}}</td>
                <td>
                  {{$r->fullName}}
                </td>
                <td>{{$r->mobileNumber}}</td>
                <td>{{$r->district}}</td>

                <td>

                  <?php 
                 
                    if($r->isForeignVisitor =="true")
                    {
                      echo "<span class='badge badge-danger'>Yes</span>";
                    }
                    else
                    {
                      echo "<span class='badge badge-primary'>No</span>"; 
                    }
                  ?>
                </td>

                <td>
                  <?php 
                  $s = explode(',', $r->physicalSymptoms);
                  foreach ($s as $key => $value) {
                    if(!empty($value))
                    {
                      echo "<span class='badge badge-danger'> $value </span>";
                    }
                    
                  }
                  ?>
                </td>
                <td>{{$r->weightedValue}}</td>
                <td class="text-center">
                  <div class="list-icons">
                    <div class="dropdown">
                      <a href="#" class="list-icons-item" data-toggle="dropdown">
                        <i class="icon-menu9"></i>
                      </a>

                      <div class="dropdown-menu dropdown-menu-right">

                        <a href="" data-toggle="modal" data-target="#markasPatient" class="dropdown-item mark" id="{{$r->id}}">
                          <i class="far fa-user" ></i> Mark as Patient 
                        </a>
                        <a href="{{route('surveyDetail',$r->id)}}" class="dropdown-item"><i class="fas fa-search-plus"></i> View Detail</a>
                        @if($pageName==='new')
                        <a href="{{route('surveyMarkRead',$r->id)}}" class="dropdown-item" ><i class="fas fa-eye"></i> Mark as Follow up</a>
                        @endif 
                        <a href="{{route('surveyHistory',$r->id)}}" class="dropdown-item"><i class="fas fa-user"></i> History</a>       
                      <!--
                        <button type="submit" class="dropdown-item delete"id="{{$r->id}}"><i class="far fa-trash-alt" ></i> Delete 
                        </button>
                      -->
                      </div>
                    </div>
                  </div>
                </td>
              </tr>
              @empty
              <tr>
                <td>No record found</td>
              </tr>
              @endforelse
            </tbody>
          </table>
<!--pag-->
          @if(count($records)>100)
          <nav aria-label="Page navigation example">
            <ul class="pagination" style="padding:10px">
              <li class="page-item">
                @if($prev <= 0) 
                <a class="page-link" disabled="">Previous</a> 
                @else 
                <a class="page-link" href="{{route('manageSurvey')}}/{{$prev}}" disabled="">Previous</a>
                @endif
                
              </li>

              <li class="page-item"><a class="page-link" href="{{route('manageSurvey')}}/{{$next}}">Next</a></li>
            </ul>
          </nav> 
          @endif         
<!--pag-->        
        </div>  
        </div>
        <!-- /basic datatable -->   

</div>


<!--add new client modal-->
<!-- Modal -->
<div class="modal fade" id="markasPatient" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-scrollable" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Mark as Patient</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
                    <form class="form" action="{{route('markAsPatient')}}" method="post">
                      @csrf
                        <input type="hidden" name="id">
                        <div class="form-group">
                          <label>Select Facility*</label>
                          <select class="form-control" id="facilityId"  name="facilityId" required>
                            @forelse($facilities as $f)
                            <option value="{{$f->id}}">{{$f->facilityName}}</option>
                            @empty
                            <option>No record found</option>
                            @endforelse

                          </select>                          
                        </div>
                        <div class="form-group">
                          <label>Comments</label>
                          <textarea class="form-control" name="comments" placeholder="Your Comments..."></textarea>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-12">
                                <button class="btn btn-primary btn-lg">Submit</button>
                            </div>
                        </div>
                    </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        
      </div>
    </div>
  </div>
</div>
<!--add new client modal end-->
<!--spinner overlay-->
@include('elements.spinner')
<!--spinner overlay end-->

@endsection

@section('js')
<script type="text/javascript">
  $(document).ready(function(){
    $(".mark").on('click',function(){
      let id = $(this).attr("id");
      $("[name='id']").val(id);
    })
  })
  //reset addForm
  function resetForm(formId)
  {
    document.getElementById(formId).reset();
  }

</script>


@endsection
