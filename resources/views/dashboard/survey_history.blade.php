@extends('/dashboard-layouts/master')

@section('content')
<div class="content">
      <!--flash message-->
      @include('elements.errors')
      <!--flash message end-->
				<!-- Basic datatable -->

				<div class="card">

					<div class="card-header header-elements-inline">
						<h5 class="card-title">
							Survey History
						</h5>
						<div class="header-elements">
							<div class="list-icons">
		                		<a class="list-icons-item" data-action="collapse"></a>
		                		<a class="list-icons-item" data-action="reload"></a>
		                		<a class="list-icons-item" data-action="remove"></a>
		                	</div>
	                	</div>
					</div>

        <div class="table-responsive" >
					<table class="table datatable-basic">
						<thead>
							<tr>
                <th>Sl No.</th>
                <th>Date</th>
                <th>Full Name</th>
                <th>Facility Name</th>
                <th>Comments</th>  

							</tr>
						</thead>
						<tbody>
              <?php $i=1;?>
              @forelse($records as $r)
 
							<tr>
                <td>{{$i++}}</td>
                <td>{{$r->date}}</td>
                <td>
                  {{$r->fullName}}
                </td>
                <td>{{$r->facilityName}}</td>
								<td>{{$r->comments}}</td>

				
							</tr>
              @empty
              <tr>
                <td>No record found</td>
              </tr>
              @endforelse
						</tbody>
					</table>
      
        </div>  
				</div>
				<!-- /basic datatable -->		

</div>


<!--add new client modal-->

<!--add new client modal end-->


@endsection

@section('js')

@endsection
