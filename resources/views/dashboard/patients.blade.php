@extends('/dashboard-layouts/master')

@section('content')
<div class="content">
      <!--flash message-->
      @include('elements.errors')
      <!--flash message end-->
        <!-- Basic datatable -->

        <div class="card">

          <div class="card-header header-elements-inline">
            <h5 class="card-title">
              Manage Patients
            </h5>
            <div class="header-elements">
              <div class="list-icons">
                        <a class="list-icons-item" data-action="collapse"></a>
                        <a class="list-icons-item" data-action="reload"></a>
                        <a class="list-icons-item" data-action="remove"></a>
                      </div>
                    </div>
          </div>

        <div class="table-responsive" >
          <table class="table datatable-basic">
            <thead>
              <tr>
                <th>Sl No.</th>
                <th>Date</th>
                <th>Full Name</th>
                <th>Present District</th>
                <th>Facility Name</th>              
              
                <th>Mobile No</th>                
                <th>Meet with Foreigner / Suspected Person</th> 

            
                <th>Status</th>
                <!--<th>Comments</th>-->
                <th class="text-center">Actions</th>
              </tr>
            </thead>
            <tbody>
              <?php $i=1;?>
              @forelse($records as $r)
 
              <tr>
                <td>{{$i++}}</td>
                <td>{{$r->patientEntryDate}}</td>
                <td>{{$r->fullName}}</td>

                <td>{{$r->district}}</td>
                <td>{{$r->facilityName}}</td>
       
                <td>{{$r->mobileNumber}}</td>
                <td>
                  <?php 
                  if($r->meetForeigner ===true)
                  {
                    echo '<span class="badge badge-danger">Yes</span>';
                  }
                  else
                  {
                    echo '<span class="badge badge-primary">No</span>';
                  }
                  ?>

                </td>           
                <td>
                  <span class="badge badge-danger">
                    {{$r->status}}
                  </span>
                </td>
                <!--<td></td>-->
                <td class="text-center">
                  <div class="list-icons">
                    <div class="dropdown">
                      <a href="#" class="list-icons-item" data-toggle="dropdown">
                        <i class="icon-menu9"></i>
                      </a>

                      <div class="dropdown-menu dropdown-menu-right">

                        <a href="{{route('editPatient',$r->id)}}" class="dropdown-item delete">
                          <i class="far fa-user" ></i> Edit 
                        </a>
                        <a class="dropdown-item change" onClick="changeStatus('<?php echo $r->id?>','<?php echo $r->status?>')" data-toggle="modal" data-target="#changeStatus"><i class="far fa-user"></i> Change Status</a>
                        <a href="{{route('patientHistory',$r->id)}}" class="dropdown-item" ><i class="far fa-user"></i> History</a>
                        <!--<a href="{{route('trackPatient',$r->id)}}" class="dropdown-item" ><i class="fas fa-eye"></i> Track</a>-->
                                  
                      <!--
                        <button type="submit" class="dropdown-item delete"id="{{$r->id}}"><i class="far fa-trash-alt" ></i> Delete 
                        </button>
                      -->
                      </div>
                    </div>
                  </div>
                </td>
              </tr>
              @empty
              <tr>
                <td>No record found</td>
              </tr>
              @endforelse
            </tbody>
          </table>
<!--pag-->
          @if(count($records)>100)
          <nav aria-label="Page navigation example">
            <ul class="pagination" style="padding:10px">
              <li class="page-item">
                @if($prev <= 0) 
                <a class="page-link" disabled="">Previous</a> 
                @else 
                <a class="page-link" href="{{route('managePatinets')}}/{{$prev}}" disabled="">Previous</a>
                @endif
                
              </li>

              <li class="page-item"><a class="page-link" href="{{route('managePatinets')}}/{{$next}}">Next</a></li>
            </ul>
          </nav> 
          @endif         
<!--pag-->        
        </div>  
        </div>
        <!-- /basic datatable -->   

</div>


<!--change status model-->
<div class="modal fade" id="changeStatus" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-scrollable" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Change Status</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
                    <form class="changeStatusForm" method="post" action="{{route('changePatientStatus')}}">
                      @csrf
                      <input type="hidden" name="patientId" value="">
                        <div class="form-group">
                          <label>Select Status</label>
                          <select class="form-control" name="status" id="status">
                                  <option value="home_quarantine">Home Quarantine</option>
                                  <option value="quarantine">Quarantine</option>
                                  <option value="isolation">Isolation</option>
                                  <option value="affected">Affected</option>
                                  <option value="recovered">Recovered</option>
                                  <option value="dead">Dead</option>
                                  <option value="released">Released</option>
                          </select>
                        </div>
                        <div class="form-group">
                            <label>Remarks</label>
                            <textarea class="form-control" name="remarks"></textarea>
                        </div>
                        <div class="form-group">
                           
                                <button class="btn btn-primary btn-lg btn-block">Change Status</button>
                          
                        </div>
                    </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        
      </div>
    </div>
  </div>
</div>
<!--add new client modal end-->
<!--change status model end-->

<!--spinner overlay-->
@include('elements.spinner')
<!--spinner overlay end-->

@endsection

@section('js')
<script type="text/javascript" src="{{url('public/js/ajax.js')}}"></script>
<script type="text/javascript">
  function changeStatus(id,status)
  {

    $('[name="patientId"]').val(id);
    $("#status").prepend(`<option value="${status}" selected>${status}</option>`);
  }
/*
$(document).ready(function(){
  $(".change").on('click',function(e){
      let id = $(this).attr('id');
      $('[name="patientId"]').val(id);
  })
})
*/
</script>


@endsection
