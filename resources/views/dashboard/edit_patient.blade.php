@extends('/dashboard-layouts/master')

@section('content')
<div class="content">
      <!--flash message-->
      @include('elements.errors')
      <!--flash message end-->
        <!-- Basic datatable -->

        <div class="card">

          <div class="card-header header-elements-inline">
            <h5 class="card-title">
              Create Patient          
            </h5>
            <div class="header-elements">
              <div class="list-icons">
                        <a class="list-icons-item" data-action="collapse"></a>
                        <a class="list-icons-item" data-action="reload"></a>
                        <a class="list-icons-item" data-action="remove"></a>
                      </div>
                    </div>
          </div>

          <div class="card-body">
                   
                    <form class="form" method="post" action="{{route('postPatient')}}">
                        @csrf
                        <input type="hidden" name="id" value="{{$records->id}}">
                        <div class="form-group row">
                            <div class="col-md-4">
                                <label>Full Name*</label>
                                <input type="" name="fullName" class="form-control" required value="{{ $records->fullName}}">
                            </div>                            
                            <div class="col-md-4">
                                <label>Father's Name*</label>
                                <input type="" name="fatherName" class="form-control"  required value="{{$records->fatherName}}">
                            </div>                            
                            <div class="col-md-4">
                                <label>Mother's Name*</label>
                                <input type="" name="motherName" class="form-control"  required value="{{$records->motherName}}">
                            </div>   
                        </div>                        


                        <label class="label"><strong>NID OR BIRTH CERTIFICATE HAVE TO FILL</strong></label>
                        <div class="form-group row">
                            <div class="col-md-6">
                                <label>National ID No:</label>
                                <input type="number" name="nidNo" class="form-control"value="{{$records->nidNo}}">
                            </div>                            
                            <div class="col-md-6 no-p">
                                <label>Birth Certificate No:</label>
                                <input type="number" name="birthCertificateNo" class="form-control" value="{{$records->birthCertificateNo}}">
                            </div>                            

                        </div>                          
                        <label class="label"><strong>PRESENT ADDRESS</strong></label>
                        <div class="form-group row">
                            <div class="col-md-4">
                                <label>Village*</label>
                                <input type="" name="village" class="form-control" required value="{{$records->presentAddress->village}}">
                            </div>                            
                            <div class="col-md-4 no-p">
                                <label>Post Office*</label>
                                <input type="" name="postOffice" class="form-control"  required value="{{$records->presentAddress->postOffice}}" required>
                            </div>                              
                            <div class="col-md-4 no-p">
                                <label>Post Code*</label>
                                <input type="number" name="postCode" class="form-control"  value="{{$records->presentAddress->postCode}}">
                            </div>                             
                            <div class="col-md-4">
                                <label>Upazila*</label>
                                <input type="" name="upazila" class="form-control"  required value="{{$records->presentAddress->upazila}}">
                            </div>                            
                            <div class="col-md-4 no-p">
                                <label>District*</label>
                                <input type="" name="district" class="form-control"  required value="{{$records->presentAddress->district}}">
                            </div>                              
                            <div class="col-md-4 no-p">
                                <label>Division*</label>
                                <select class="form-control" name="division" required="">
                                    <option value="{{$records->presentAddress->division}}">{{$records->presentAddress->division}}</option>
                                    @forelse($divisions as $d)
                                    <option value="{{$d->name}}">
                                        {{$d->name}}
                                    </option>  
                                    @empty
                                    <option>No record found</option>
                                    @endforelse
                                </select>
                            </div>                            

                        </div>   
                       <input type="checkbox" id="same" name="same">
                        <label for="same">Same as Present Address</label>
                        <br>
                        <label class="label">
                            <strong>PERMANENT ADDRESS</strong>
                        </label>
                        <div class="form-group row">
                            <div class="col-md-4">
                                <label>Village*</label>
                                <input type="" id="village" name="per_village" class="form-control" required value="{{old('village')}}">
                            </div>                            
                            <div class="col-md-4 no-p">
                                <label>Post Office*</label>
                                <input type="" name="per_postOffice" id="postOffice" class="form-control" value="{{old('postOffice')}}" required>
                            </div>                              
                            <div class="col-md-4 no-p">
                                <label>Post Code</label>
                                <input type="number" name="per_postCode" id="postCode" class="form-control"  value="{{old('postCode')}}">
                            </div>                             
                            <div class="col-md-4">
                                <label>Upazila*</label>
                                <input type="" name="per_upazila" class="form-control" id="upazila"  required value="{{old('upazila')}}">
                            </div>                            
                            <div class="col-md-4 no-p">
                                <label>District*</label>
                                <input type="" id="district" name="per_district" class="form-control"  required value="{{old('district')}}">
                            </div>                              
                            <div class="col-md-4 no-p">
                                <label>Division*</label>
                                <select class="form-control" name="per_division" id="division" required="">
                                    <option>Select</option>
                                    @forelse($divisions as $d)
                                    @if(old('division')==$d->name)
                                    <option value="{{$d->name}}" selected="">
                                      {{$d->name}}</option>
                                    @else
                                    <option value="{{$d->name}}" >
                                      {{$d->name}}</option>
                                      @endif  
                                    @empty
                                    <option>No record found</option>
                                    @endforelse
                                </select>
                            </div>  
                           
                        </div> 

                        <div class="form-group row">
                            <div class="col-md-6">
                                <label>Mobile No:*</label>
                                <input type="number" name="mobileNumber" class="form-control" minlength="11" required value="{{$records->mobileNumber}}">
                            </div>                            
                            <div class="col-md-6 no-p">
                                <label>Date of Birth* (1990-01-01 mm-dd-yyyy)</label>
                                <input type="" name="dateOfBirth" class="form-control" required value="{{$records->dateOfBirth}}">
                            </div>                            

                        </div>                         

                        <div class="form-group row">
                            <div class="col-md-4">
                                <label>Gender*</label>
                                <select class="form-control" name="gender" required="">

                                    <option value="Male"
                                    <?php 
                                    if($records->gender=='Male')
                                    {
                                      echo "selected";
                                    }
                                      ?>
                                    >Male</option>
                                    <option value="Female"
                                    <?php 
                                    if($records->gender=='Female')
                                    {
                                      echo "selected";
                                    }
                                      ?>
                                    >Female</option>                                    <option value="Female"
                                    <?php 
                                    if($records->gender=='Other')
                                    {
                                      echo "selected";
                                    }
                                      ?>
                                    >Other</option>
                                </select>
                            </div>                            
                            <div class="col-md-4">
                                <label>Blood Group*</label>
                                <select class="form-control" name="bloodGroup" required>
                                    <option value="{{$records->bloodGroup}}">
                                        {{$records->bloodGroup}}
                                    </option>
                                    <option value="A+">A+</option>
                                    <option value="A-">A-</option>
                                    <option value="B+">B+</option>
                                    <option value="B-">B-</option>
                                    <option value="AB+">AB+</option>
                                    <option value="AB-">AB-</option>
                                    <option value="O+">O+</option>
                                    <option value="O-">O-</option>
                                </select>
                            </div>                              
                            <div class="col-md-4">
                                <label>Number of Days Sick*</label>
                                <input type="number" name="numOfDaysSick" class="form-control"  required value="{{$records->numOfDaysSick}}">
                            </div>                            

                        </div> 

                        <div class="form-group row">                               
                            <div class="col-md-12">
                                <label>Physical Symptoms* (at least one symptom)</label>
                                <br>

                                @forelse($symptoms as $k=> $s)
                                <!--match with exists value-->
                                <?php 

                                 foreach ($records->phySymMapList as $key => $value)
                                 {
                                    if($s->id ==$value->physicalSymptomInfoId)
                                    {
                                    unset($symptoms[$k]);
                                    echo 
                                    '
                                    <div class="form-check form-check-inline col-md-10">
                                      <input class="form-check-input" type="checkbox" id="'.$s->name.'" value="'.$s->id.'" name="physicalSymptoms[]" checked>
                                      <label class="form-check-label col-md-4" for="'.$s->name.'">'.$s->name.'</label>

                                      <input type="number" name="days[]" class="form-control col-md-4" min="0" placeholder="Number of days" value="'.$value->numOfDaysSick.'">
                                    </div>
                                    ';
                                        break;
                                    }
                                    else
                                    {
                                        
                                    }

                                 }
                                ?>

                                @empty
                                
                                @endforelse

                                @forelse($symptoms as $s)
                                    <div class="form-check form-check-inline col-md-10">
                                      <input class="form-check-input" type="checkbox" id="{{$s->name}}" value="{{$s->id}}" name="physicalSymptoms[]">
                                      <label class="form-check-label col-md-4" for="{{$s->name}}">{{$s->name}}</label>

                                      <input type="number" name="days[]" class="form-control col-md-4" min="0" placeholder="Number of days" value=""/>
                                    </div>  
                                @empty

                                @endforelse

                            </div>   
                        </div>        

                        <div class="form-group row">
                            <div class="col-md-4">
                                <label>Meet with Foreigners / Suspected Person* </label>

                                <div class="form-check">
                                  <input class="form-check-input" type="radio" name="isForeignVisitor" id="yes" value="true" 
                                  <?php 
                                  if($records->meetForeigner ===true)
                                  {
                                    echo "checked";
                                  }
                                    ?> 
                                    required>
                                  <label class="form-check-label" for="yes">
                                    Yes
                                  </label>
                                </div>                               
                                 <div class="form-check">
                                  <input class="form-check-input" type="radio" name="isForeignVisitor" id="no" value="false"
                                  <?php 
                                  if($records->meetForeigner ==false)
                                  {
                                    echo "checked";
                                  }
                                    ?> 
                                   required>
                                  <label class="form-check-label" for="no">
                                    No
                                  </label>
                                </div>
                            </div>   
                        </div> 
                        <div class="form-group row">
                            <div class="col-md-6">
                                <label>Remarks</label>
                                <textarea class="form-control" name="remarks">{{$records->remarks}}</textarea>
                            </div> 
                            <div class="col-md-6">
                                <label>Recommendation</label>
                                <textarea class="form-control" name="recommendation">{{$records->recommendation}}</textarea>
                            </div>                              
                        </div>                         
                       

                        
                        <div class="form-group row">
                            <div class="col-md-12">
                                <button class="btn btn-primary btn-lg">Submit Information</button>
                            </div>
                        </div>
                    </form>
          </div>  
        </div>
        <!-- /basic datatable -->   

</div>

@endsection

@section('js')
<script type="text/javascript">
     $(document).ready(function(){

        $('input[name="same"]').click(function(){

            if($(this).is(":checked")){
                let village = $('[name="village"]').val();
                let dis = $('[name="district"]').val();
                let div = $('[name="division"]').val();
                let upazila = $('[name="upazila"]').val();
                let poffice = $('[name="postOffice"]').val();
                let pocode = $('[name="postCode"]').val();                

                $('#village').val(village);
                $('#district').val(dis);
                $('#division').val(div);
                $('#upazila').val(upazila);
                $('#postOffice').val(poffice);
                $('#postCode').val(pocode);
            }

            else if($(this).is(":not(:checked)")){
                $('#village').val('');
                $('#district').val('');
                $('#division').val('');
                $('#upazila').val('');
                $('#postOffice').val('');
                $('#postCode').val('');

            }

        });

        //district as per div

    });    
</script>
@endsection
