@extends('/dashboard-layouts/master')

@section('content')
<div class="content">
      <!--flash message-->
      @include('elements.errors')
      <!--flash message end-->
				<!-- Basic datatable -->

				<div class="card">

					<div class="card-header header-elements-inline">
						<h5 class="card-title">
							<a href="#"  onClick="resetForm('addForm')" data-toggle="modal" data-target="#addUser" class="btn-primary btn"><i class="fas fa-plus-circle"></i> Add Authority User</a>
						</h5>
						<div class="header-elements">
							<div class="list-icons">
		                		<a class="list-icons-item" data-action="collapse"></a>
		                		<a class="list-icons-item" data-action="reload"></a>
		                		<a class="list-icons-item" data-action="remove"></a>
		                	</div>
	                	</div>
					</div>

        <div class="table-responsive" >

					<table class="table datatable-basic">
						<thead>
							<tr>
                <th>Sl No.</th>
								<th>Name</th>
                <th>Email</th>
                <th>Village</th>
                <th>Upazila</th>
                <th>Thana</th>                
                <th>District</th>                
                <th>Division</th>                
								<th>Status</th>
								<th class="text-center">Actions</th>
							</tr>
						</thead>
						<tbody>
              <?php $i=1;?>
              @forelse($records as $r)
              @if($r->role==="ROLE_MONITOR")
              <?php 
                $id= $r->id;
                $username= $r->username;
                $email= $r->email;
                $village= $r->address->village;
                $postOffice= $r->address->postOffice;
                $postCode= $r->address->postCode;
                $thana= $r->address->thana;
                $upazila= $r->address->upazila;
                $district= $r->address->district;
                $division= $r->address->division;
                $activated= $r->activated;                

              ?>  
							<tr>
                <td>{{$i++}}</td>
								<td>{{$username}}</td>
								<td>{{$email}}</td>
                <td>{{$village}}</td>
                <td>{{$upazila}}</td>
                <td>{{$thana}}</td>
                <td>{{$district}}</td>
                <td>{{$division}}</td>
								<td>
                  <?php 
                  if($activated===true)
                  {
                    echo '<label class="badge badge-primary">Active </label>';
                  }
                  else
                  {
                    echo '<label class="badge badge-danger">Deactive</label>';
                  }
                  ?>
      
                </td>
								

								<td class="text-center">
									<div class="list-icons">
										<div class="dropdown">
											<a href="#" class="list-icons-item" data-toggle="dropdown">
												<i class="icon-menu9"></i>
											</a>

											<div class="dropdown-menu dropdown-menu-right">
                        <?php 

                        echo '
                          <a href="#" onClick="edit('.$id.',\''.$username.'\' ,\''.$email.'\',\''.$activated.'\',\''.$village.'\',\''.$postOffice.'\',\''.$postCode.'\',\''.$thana.'\',\''.$upazila.'\',\''.$district.'\',\''.$division.'\')" data-toggle="modal" data-target="#updateUser" class="dropdown-item"><i class="fas fa-edit"></i> Edit</a>
                        ';
                        ?>

                    
                   
                      <!--  <button type="submit" class="dropdown-item delete"id="{{$r->id}}"><i class="far fa-trash-alt" ></i> Delete 
                        </button>
											-->
											</div>
										</div>
									</div>
								</td>
							</tr>
              @endif
              @empty
              <tr>
                <td>No record found</td>
              </tr>
              @endforelse
						</tbody>
					</table>
<!--pagination-->    
          @if(count($records)>100)
          <nav aria-label="Page navigation example">
            <ul class="pagination" style="padding:10px">
              <li class="page-item">
                @if($prev <= 0) 
                <a class="page-link" disabled="">Previous</a> 
                @else 
                <a class="page-link" href="{{url('manage-users')}}/{{$prev}}" disabled="">Previous</a>
                @endif
                
              </li>

              <li class="page-item"><a class="page-link" href="{{url('manage-users')}}/{{$next}}">Next</a></li>
            </ul>
          </nav> 
          @endif       
<!--pagination end-->          
        </div>  
				</div>
				<!-- /basic datatable -->		

</div>
<!--add new client modal-->
<!-- Modal -->
<div class="modal fade" data-keyboard="false" data-backdrop="static" id="addUser" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-scrollable" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Add Authority User</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      		<form class="form-horizontal" method="post" action="{{route('createAuthority')}}" id="addForm">
            @csrf
            

            <div class="form-group">
              <label>User Name</label>
              <input class="form-control" name="username" required>
            </div> 

            <div class="form-group">
              <label>Email</label>
              <input type="email" class="form-control" name="email" required>
            </div>

            <div class="form-group">
              <label>Password</label>
              <input type="password" class="form-control" name="password" minlength="6">
            </div> 

            <div class="form-group">
              <label>Village*</label>
              <input type="" class="form-control" name="village" required>
            </div> 

            <div class="form-group">
              <label>Post Office*</label>
              <input type="" class="form-control" name="postOffice" required>
            </div>

            <div class="form-group">
              <label>Post Code*</label>
              <input type="number" min="1" class="form-control" name="postCode" required>
            </div>             

            <div class="form-group">
              <label>Thana*</label>
              <input type="" class="form-control" name="thana" required>
            </div>             

            <div class="form-group">
              <label>Upazila*</label>
              <input type="" class="form-control" name="upazila" required>
            </div>             

            <div class="form-group">
              <label>District*</label>
              <input type="" class="form-control" name="district" required>
            </div>             

            <div class="form-group">
              <label>Division*</label>
              <select class="form-control preDiv" name="division" required>
                <option value="">Select</option>
                    @forelse($divisions as $d)
                <option value="{{$d->name}}">{{$d->name}}</option>
                @empty
                <option>No record found</option>
                @endforelse
              </select>
            </div>            

            <div class="form-group">
              <label>Status</label>
              <select class="form-control" name="activated">
                  <option value="true">Active</option>
                  <option value="false">Deactive</option>
              </select>
            </div>


      			<div class="form-group" id="addingResponse">
              
            </div>     			
      			<div class="form-group">
      				
      				<button class="btn-primary btn">Add Athority User</button>
      			</div>
      			

      		</form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        
      </div>
    </div>
  </div>
</div>
<!--add new client modal end-->

<!--add new client modal-->
<!-- Modal -->
<div class="modal fade" data-keyboard="false" data-backdrop="static" id="updateUser" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-scrollable" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Update Authority User</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <form class="form-horizontal" method="post" action="{{route('createAuthority')}}" id="addForm">
            @csrf
                     
            <input type="hidden" name="id">
            <div class="form-group">
              <label>User Name</label>
              <input class="form-control" name="username" required>
            </div> 

            <div class="form-group">
              <label>Email</label>
              <input type="email" class="form-control" name="email" required>
            </div>

            <div class="form-group">
              <label>Village*</label>
              <input type="" class="form-control" name="village" required>
            </div> 

            <div class="form-group">
              <label>Post Office*</label>
              <input type="" class="form-control" name="postOffice" required>
            </div>

            <div class="form-group">
              <label>Post Code*</label>
              <input type="number" min="1" class="form-control" name="postCode" required>
            </div>             

            <div class="form-group">
              <label>Thana*</label>
              <input type="" class="form-control" name="thana" required>
            </div>             
            <div class="form-group">
              <label>Upazila*</label>
              <input type="" class="form-control" name="upazila" required>
            </div> 
            <div class="form-group">
              <label>District*</label>
              <input type="" class="form-control" name="district" required>
            </div>            

            <div class="form-group">
              <label>Division*</label>
              <select class="form-control preDiv" id="division" name="division" required>
                <option value="">Select</option>
                    @forelse($divisions as $d)
                <option value="{{$d->name}}">{{$d->name}}</option>
                @empty
                <option>No record found</option>
                @endforelse
              </select>
            </div>            

            <div class="form-group">
              <label>Status</label>
              <select class="form-control" name="activated">
                  <option value="true">Active</option>
                  <option value="false">Deactive</option>
              </select>
            </div>


            <div class="form-group" id="addingResponse">
              
            </div>          
            <div class="form-group">
              
              <button class="btn-primary btn">Update Athority User</button>
            </div>
            

          </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        
      </div>
    </div>
  </div>
</div>
<!--add new client modal end-->
<!--spinner overlay-->
@include('elements.spinner')
<!--spinner overlay end-->

@endsection

@section('js')
<script type="text/javascript">

  function edit(id,name,email,activated,village,postOffice,postCode,thana,upazila,district,division){
 
      $('[name="id"]').val(id);
      $('[name="username"]').val(name);
      $('[name="email"]').val(email);
      $('[name="village"]').val(village);
      $('[name="postOffice"]').val(postOffice);
      $('[name="postCode"]').val(postCode);
      $('[name="thana"]').val(thana);
      $('[name="upazila"]').val(upazila);
      $('[name="district"]').val(district);
      $("#division").prepend(`<option value="${division}" selected>${division}</option>`);
      if(activated==1)
      {
        $("#activated").prepend(`<option value='true'>Activate</option>`);
      }
      else
      {
        $("#activated").prepend(`<option value='false'>Deactivate</option>`);
      }
  }
  //reset addForm
  function resetForm(formId)
  {
    document.getElementById(formId).reset();
  }
</script>


@endsection
