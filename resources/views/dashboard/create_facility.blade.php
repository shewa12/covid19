@extends('/dashboard-layouts/master')

@section('content')
<div class="content">
      <!--flash message-->
      @include('elements.errors')
      <!--flash message end-->
        <!-- Basic datatable -->

        <div class="card">

          <div class="card-header header-elements-inline">
            <h5 class="card-title">
              Create Facility          
            </h5>
            <div class="header-elements">
              <div class="list-icons">
                        <a class="list-icons-item" data-action="collapse"></a>
                        <a class="list-icons-item" data-action="reload"></a>
                        <a class="list-icons-item" data-action="remove"></a>
                      </div>
                    </div>
          </div>

          <div class="card-body">
             
                    <form class="form" method="post" action="{{route('postFacility')}}">
                      @csrf
                        <div class="form-group row">
                            <div class="col-md-4">
                                <label>Facility Name*</label>
                                <input type="" name="facilityName" class="form-control" value="{{old('facilityName')}}" required>
                            </div>                            
                            <div class="col-md-4">
                                <label>Number of Bed*</label>
                                <input type="number" name="numberOfBeds" class="form-control" value="{{old('numberOfBeds')}}" required>
                            </div>                            
                            <div class="col-md-4">
                                <label>Number of Doctor*</label>
                                <input type="number" name="numberOfDoctors" value="{{old('numberOfDoctors')}}" class="form-control" required>
                            </div>
                        </div>                         

                        <div class="form-group row">
                            <div class="col-md-4">
                                <label>Number of Nurse*</label>
                                <input type="number" name="numOfNurse" class="form-control" min="0" value="{{old('numOfNurse')}}" required>
                            </div>                            
                            <div class="col-md-4">
                                <label>Number of Ventilator*</label>
                                <input type="number" name="numOfVentilator" class="form-control" min="0" value="{{old('numOfVentilator')}}" required>
                            </div>                           
                            <div class="col-md-4">
                                <label>Number of Vaccine*</label>
                                <input type="number" name="numOfVaccine" class="form-control" min="0" value="{{old('numOfVaccine')}}" required>
                            </div> 
                        </div>                          

                        <div class="form-group row">
                            <div class="col-md-4">
                                <label>Number of ICU Bed*</label>
                                <input type="number" name="numOfIcu" value="{{old('numOfIcu')}}" class="form-control" required>
                            </div>                             
                            <div class="col-md-4">
                                <label>Number of Technician*</label>
                                <input type="number" name="numberOfVolunteers" class="form-control" value="{{old('numberOfVolunteers')}}" required="">
                            </div>                            
                            <div class="col-md-4">
                                <label>Help Line*</label>
                                <input type="number" name="helpLineNumber" class="form-control" required value="{{old('helpLineNumber')}}">
                            </div>                            

                        </div>                        

                        <label class="label"><strong>PRESENT ADDRESS</strong></label>
                        <div class="form-group row">
                            <div class="col-md-4">
                                <label>Village*</label>
                                <input type="" name="village" class="form-control" value="{{old('village')}}" required>
                            </div>                            
                            <div class="col-md-4 no-p">
                                <label>Post Office*</label>
                                <input type="" name="postOffice" class="form-control" value="{{old('postOffice')}}" required>
                            </div>                              
                            <div class="col-md-4 no-p">
                                <label>Post Code*</label>
                                <input type="number" name="postCode" class="form-control" value="{{old('postCode')}}" required>
                            </div>                             
                            <div class="col-md-4">
                                <label>Upazilla*</label>
                                <input type="" name="upazila" class="form-control" value="{{old('upazila')}}" required>
                            </div>                            
                            <div class="col-md-4 no-p">
                                <label>District*</label>
                                <input type="" name="district" class="form-control" value="{{old('district')}}" required>
                            </div>                              
                            <div class="col-md-4 no-p">
                                <label>Division*</label>
                                <select class="form-control" name="division" required="">
                                    <option value="">Select</option>
                                    @forelse($divisions as $d)
                                    
                                    @if (old('division') == $d->name)
                                          <option value="{{ $d->name }}" selected>{{ $d->name }}</option>
                                    @else
                                          <option value="{{ $d->name }}">{{ $d->name }}</option>
                                    @endif
                                    

                                    @empty
                                    <option>No record found</option>
                                    @endforelse
                                </select>
                            </div>                            

                        </div>   


                        
                        <div class="form-group row">
                            <div class="col-md-12">
                                <button class="btn btn-primary btn-lg">Submit Information</button>
                            </div>
                        </div>
                    </form>
          </div>  
        </div>
        <!-- /basic datatable -->   

</div>

<!--spinner overlay-->
@include('elements.spinner')
<!--spinner overlay end-->

@endsection

@section('js')
<script type="text/javascript">
    
</script>
@endsection
