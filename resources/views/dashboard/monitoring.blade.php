@extends('/dashboard-layouts/master')

@section('content')
<div class="content">
      <!--flash message-->
      @include('elements.errors')
      <!--flash message end-->
        <!-- Basic datatable -->

        <div class="card">

          <div class="card-header header-elements-inline">
            <h5 class="card-title">
              Monitoring History
            </h5>
            <div class="header-elements">
              <div class="list-icons">
                        <a class="list-icons-item" data-action="collapse"></a>
                        <a class="list-icons-item" data-action="reload"></a>
                        <a class="list-icons-item" data-action="remove"></a>
                      </div>
                    </div>
          </div>

        <div class="table-responsive" >
          <table class="table datatable-basic">
            <thead>
              <tr>
                <th>Sl No.</th>
                <th>Time</th>
                <th>Name</th>
                <th>Mobile No.</th>
                <th>Present Address</th>
                <th>NID No</th>
                <th>Last Quarantine Date</th>
                <th>Status</th>
                <th>Message</th>
                <th>Comments</th>

              </tr>
            </thead>
            <tbody>
              <?php $i=1;?>
              @forelse($records as $r)
 
              <tr>
                <td>{{$i++}}</td>
                <td>{{$r->time}}</td>
                <td>{{$r->patientName}}</td>
                <td>{{$r->phoneNumber}}</td>
                <td>{{$r->presentAddress}}</td>
                <td>{{$r->nid}}</td>
                <td>{{$r->lastQuarantineDate}}</td>
                <td>
                  <span class="badge badge-warning">
                    {{$r->typeStatus}}
                  </span>
                </td>
                <td>{{$r->largeMsg}}</td>
                <td>{{$r->comment}}</td>

              </tr>
              @empty
              <tr>
                <td>No record found</td>
              </tr>
              @endforelse
            </tbody>
          </table>
<!--pagination-->    
          @if(count($records)>100)
          <nav aria-label="Page navigation example">
            <ul class="pagination" style="padding:10px">
              <li class="page-item">
                @if($prev <= 0) 
                <a class="page-link" disabled="">Previous</a> 
                @else 
                <a class="page-link" href="{{url('/quarantine-monitoring')}}/{{$prev}}" disabled="">Previous</a>
                @endif
                
              </li>

              <li class="page-item"><a class="page-link" href="{{url('quarantine-monitoring')}}/{{$next}}">Next</a></li>
            </ul>
          </nav> 
          @endif       
<!--pagination end-->                  
        </div>  
        </div>
        <!-- /basic datatable -->   

</div>


<!--add new client modal-->
<!-- Modal -->
<div class="modal fade" id="updateFacility" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-scrollable" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Update Facility</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
                    <form class="form" method="post" action="{{route('postFacility')}}">
                      @csrf
                      <input type="hidden" name="id" value="">
                        <div class="form-group row">
                            <div class="col-md-4">
                                <label>Facility Name*</label>
                                <input type="" name="facilityName" class="form-control" required>
                            </div>                            
                            <div class="col-md-4">
                                <label>Number of Bed*</label>
                                <input type="number" name="numberOfBeds" class="form-control" required>
                            </div>                            
                            <div class="col-md-4">
                                <label>Number of Doctor*</label>
                                <input type="number" name="numberOfDoctors" class="form-control">
                            </div>
                        </div>                          

                        <div class="form-group row">
                            <div class="col-md-4">
                                <label>Number of ICU*</label>
                                <input type="number" name="numOfIcu" class="form-control" required>
                            </div>                             
                            <div class="col-md-4">
                                <label>Number of Volunteer*</label>
                                <input type="number" name="numberOfVolunteers" class="form-control">
                            </div>                            
                            <div class="col-md-4">
                                <label>Help Line</label>
                                <input type="number" name="helpLineNumber" class="form-control">
                            </div>                            

                        </div>                        

                        <label class="label"><strong>PRESENT ADDRESS</strong></label>
                        <div class="form-group row">
                            <div class="col-md-4">
                                <label>Village*</label>
                                <input type="" name="village" class="form-control" required>
                            </div>                            
                            <div class="col-md-4 no-p">
                                <label>Post Office*</label>
                                <input type="" name="postOffice" class="form-control" required>
                            </div>                              
                            <div class="col-md-4 no-p">
                                <label>Post Code*</label>
                                <input type="" name="postCode" class="form-control"required>
                            </div>                             
                            <div class="col-md-4">
                                <label>Upazilla*</label>
                                <input type="" name="upazila" class="form-control"required>
                            </div>                            
                            <div class="col-md-4 no-p">
                                <label>District*</label>
                                <input type="" name="district" class="form-control" required>
                            </div>                              
                            <div class="col-md-4 no-p">
                                <label>Division*</label>
                                <input type="" name="division" class="form-control" required>
                            </div>                            

                        </div>   


                        
                        <div class="form-group row">
                            <div class="col-md-12">
                                <button class="btn btn-primary btn-lg">Submit Information</button>
                            </div>
                        </div>
                    </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        
      </div>
    </div>
  </div>
</div>
<!--add new client modal end-->
<!--change status model-->
<div class="modal fade" id="changeStatus" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-scrollable" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Change Status</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
                    <form class="changeStatusForm" method="post" action="{{route('changePatientStatus')}}">
                      @csrf
                      <input type="hidden" name="patientId" value="">
                        <div class="form-group">
                          <label>Select Status</label>
                          <select class="form-control" name="status">
                            <option value="">Select</option>
                            <option value="Recovered">Recovered</option>
                            <option value="Home-Quarantine">Home Quarantine</option>
                            <option value="Quarantine">Quarantine</option>
                            <option value="Isolation">Isolation</option>
                            <option value="Dead">Dead</option>
                          </select>
                        </div>
                        <div class="form-group">
                            <label>Remarks</label>
                            <textarea class="form-control" name="remarks"></textarea>
                        </div>
                        <div class="form-group">
                           
                                <button class="btn btn-primary btn-lg btn-block">Change Status</button>
                          
                        </div>
                    </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        
      </div>
    </div>
  </div>
</div>
<!--add new client modal end-->
<!--change status model end-->

<!--spinner overlay-->
@include('elements.spinner')
<!--spinner overlay end-->

@endsection

@section('js')



@endsection
