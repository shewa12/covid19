@extends('/dashboard-layouts/master')

@section('content')
<div class="content">
      <!--flash message-->
      @include('elements.errors')
      <!--flash message end-->
				<!-- Basic datatable -->

				<div class="card">

					<div class="card-header header-elements-inline">
						<h5 class="card-title">
							Manage Facilities
						</h5>
						<div class="header-elements">
							<div class="list-icons">
		                		<a class="list-icons-item" data-action="collapse"></a>
		                		<a class="list-icons-item" data-action="reload"></a>
		                		<a class="list-icons-item" data-action="remove"></a>
		                	</div>
	                	</div>
					</div>

        <div class="table-responsive" >
					<table class="table datatable-basic">
						<thead>
							<tr>
                <th>Sl No.</th>
								<th>Name</th>
                <th>Districts</th>
                <th>Division</th>
                <th>Available Beds</th>
                <th>Available ICU Beds</th>
                <th>Beds</th>
                <th>ICU Beds</th>                
								<th>Doctor</th>
                <th>Nurse</th>
                <th>Ventilator</th>
                <th>Vaccine</th>
                <th>Technician</th>
                <th>Help Line</th>
                <th>Address</th>
								<th class="text-center">Actions</th>
							</tr>
						</thead>
						<tbody>
              <?php $i=1;?>
              @forelse($records as $r)
              <?php 
                $id= $r->id;
                $name= $r->facilityName;
                $beds= $r->numberOfBeds;
                $icu= $r->numOfIcu;                
                $doctors= $r->numberOfDoctors;
                $volunteers= $r->numberOfVolunteers;

                $nurse= $r->numOfNurse;
                $ventilator= $r->numOfVentilator;
                $vaccine= $r->numOfVaccine;

                $help= $r->helpLineNumber;
                $village = $r->address->village;
                $poffice = $r->address->postOffice;
                $pcode = $r->address->postCode;
                $upazila = $r->address->upazila;
                $district = $r->address->district;
                $division = $r->address->division;

              ?>  
							<tr>
                <td>{{$i++}}</td>
								<td>{{$name}}</td>
                <td>{{$district}}</td>
                <td>{{$division}}</td>
                <td>{{$r->numberOfAvailableBeds}}</td>
                <td>{{$r->numOfAvailableIcu}}</td>
								<td>{{$beds}}</td>
								<td>{{$icu}}</td>
								<td>{{$doctors}}</td>
                <td>{{$nurse}}</td>
                <td>{{$ventilator}}</td>
                <td>{{$vaccine}}</td>
                <td>{{$volunteers}}</td>
                <td>{{$help}}</td>
                <td>
                  Village: {{$village}},
                  Post Office: {{$poffice}},
                  Post Code: {{$pcode}},
                  Upazila: {{$upazila}},
                  Disctrict: {{$district}},
                  Division: {{$division}}
                </td>
								<td class="text-center">
									<div class="list-icons">
										<div class="dropdown">
											<a href="#" class="list-icons-item" data-toggle="dropdown">
												<i class="icon-menu9"></i>
											</a>

											<div class="dropdown-menu dropdown-menu-right">
                        <?php 

                        echo '
                          <a href="#" onClick="edit('.$id.',\''.$name.'\' ,\''.$beds.'\',\''.$icu.'\',\''.$doctors.'\',\''.$volunteers.'\',\''.$village.'\',\''.$poffice.'\',\''.$pcode.'\',\''.$upazila.'\',\''.$district.'\',\''.$division.'\',\''.$help.'\',\''.$nurse.'\',\''.$ventilator.'\',\''.$vaccine.'\')" data-toggle="modal" data-target="#updateFacility" class="dropdown-item"><i class="fas fa-edit"></i> Edit</a>
                        ';
                        ?>

                    
                      <!--
                        <button type="submit" class="dropdown-item delete"id="{{$r->id}}"><i class="far fa-trash-alt" ></i> Delete 
                        </button>
											-->
											</div>
										</div>
									</div>
								</td>
							</tr>
              @empty
              <tr>
                <td>No record found</td>
              </tr>
              @endforelse
						</tbody>
					</table>
<!--pag-->
          @if(count($records)>100)
          <nav aria-label="Page navigation example">
            <ul class="pagination" style="padding:10px">
              <li class="page-item">
                @if($prev <= 0) 
                <a class="page-link" disabled="">Previous</a> 
                @else 
                <a class="page-link" href="{{url('facilities')}}/{{$prev}}" disabled="">Previous</a>
                @endif
                
              </li>

              <li class="page-item"><a class="page-link" href="{{url('facilities')}}/{{$next}}">Next</a></li>
            </ul>
          </nav> 
          @endif         
<!--pag-->        
        </div>  
				</div>
				<!-- /basic datatable -->		

</div>


<!--add new client modal-->
<!-- Modal -->
<div class="modal fade" data-keyboard="false" data-backdrop="static" id="updateFacility" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-scrollable" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Update Facility</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
                    <form class="form" method="post" action="{{route('postFacility')}}">
                      @csrf
                      <input type="hidden" name="id" value="">
                        <div class="form-group row">
                            <div class="col-md-4">
                                <label>Facility Name*</label>
                                <input type="" name="facilityName" class="form-control" required>
                            </div>                            
                            <div class="col-md-4">
                                <label>Number of Bed*</label>
                                <input type="number" name="numberOfBeds" class="form-control" required>
                            </div>                            
                            <div class="col-md-4">
                                <label>Number of Doctor*</label>
                                <input type="number" name="numberOfDoctors" class="form-control">
                            </div>
                        </div>                          
                        <div class="form-group row">
                            <div class="col-md-4">
                                <label>Number of Nurse*</label>
                                <input type="number" name="numOfNurse" class="form-control" min="0" value="{{old('numberOfNurse')}}" required>
                            </div>                            
                            <div class="col-md-4">
                                <label>Number of Ventilator*</label>
                                <input type="number" name="numOfVentilator" class="form-control" min="0" value="{{old('numberOfVentilator')}}" required>
                            </div>                           
                            <div class="col-md-4">
                                <label>Number of Vaccine*</label>
                                <input type="number" name="numOfVaccine" class="form-control" min="0" value="{{old('numberOfVaccine')}}" required>
                            </div> 
                        </div> 
                        <div class="form-group row">
                            <div class="col-md-4">
                                <label>Number of ICU Bed*</label>
                                <input type="number" name="numOfIcu" class="form-control" required>
                            </div>                             
                            <div class="col-md-4">
                                <label>Number of Volunteer*</label>
                                <input type="number" name="numberOfVolunteers" class="form-control">
                            </div>                            
                            <div class="col-md-4">
                                <label>Help Line*</label>
                                <input type="number" name="helpLineNumber" class="form-control" required>
                            </div>                            

                        </div>                        

                        <label class="label"><strong>PRESENT ADDRESS</strong></label>
                        <div class="form-group row">
                            <div class="col-md-4">
                                <label>Village*</label>
                                <input type="" name="village" class="form-control" required>
                            </div>                            
                            <div class="col-md-4 no-p">
                                <label>Post Office*</label>
                                <input type="" name="postOffice" class="form-control" required>
                            </div>                              
                            <div class="col-md-4 no-p">
                                <label>Post Code*</label>
                                <input type="" name="postCode" class="form-control"required>
                            </div>                             
                            <div class="col-md-4">
                                <label>Upazilla*</label>
                                <input type="" name="upazila" class="form-control"required>
                            </div>                            
                            <div class="col-md-4 no-p">
                                <label>District*</label>
                                <input type="" name="district" class="form-control" required>
                            </div>                              
                            <div class="col-md-4 no-p">
                                <label>Division*</label>
                                <select class="form-control" name="division" id="division" required="">
                                    <option value="">Select</option>
                                    @forelse($divisions as $d)

                                      <option value="{{ $d->name }}">{{ $d->name }}</option>

                                    @empty
                                    <option>No record found</option>
                                    @endforelse
                                </select>
                            </div>                            

                        </div>   


                        
                        <div class="form-group row">
                            <div class="col-md-12">
                                <button class="btn btn-primary btn-lg">Update Information</button>
                            </div>
                        </div>
                    </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        
      </div>
    </div>
  </div>
</div>
<!--add new client modal end-->
<!--spinner overlay-->
@include('elements.spinner')
<!--spinner overlay end-->

@endsection

@section('js')
<script type="text/javascript">

  function edit(id,name,beds,icu,doctors,volunteers,village,poffice,pcode,upazila,district,division,help,nurse,ventilator,vaccine){

      $('[name="id"]').val(id);
      $('[name="facilityName"]').val(name);
      $('[name="numberOfBeds"]').val(beds);
      $('[name="numOfIcu"]').val(icu);
      $('[name="numberOfDoctors"]').val(doctors); 
      $('[name="numberOfVolunteers"]').val(volunteers); 

      $('[name="numOfNurse"]').val(nurse);
      $('[name="numOfVentilator"]').val(ventilator); 
      $('[name="numOfVaccine"]').val(vaccine); 

      $('[name="village"]').val(village); 
      $('[name="postOffice"]').val(poffice); 
      $('[name="postCode"]').val(pcode); 
      $('[name="upazila"]').val(upazila); 
      $('[name="district"]').val(district); 
      $('#division').prepend(`<option value="${division}" selected>${division}</option>`); 
      $('[name="helpLineNumber"]').val(help); 
  }
  //reset addForm
  function resetForm(formId)
  {
    document.getElementById(formId).reset();
  }
</script>


@endsection
