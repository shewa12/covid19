@include('landing_header')
    <!-- banner part start-->
  
    <section class="banner_part">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-5 col-xl-5">
                    <div class="banner_text">
                        <div class="banner_text_iner">
                            <h5>Coronavirus (COVID-19)</h5>
                            <h1>What is Coronavirus?</h1>
                            <p>Coronaviruses (CoV) are a large family of viruses that cause illness ranging from the common cold to more severe diseases such as Middle East Respiratory Syndrome (MERS-CoV) and Severe Acute Respiratory Syndrome (SARS-CoV).

                            Coronavirus disease (COVID-19) is a new strain that was discovered in 2019 and has not been previously identified in humans. </p>
                            <a href="{{route('surveyForm')}}" class="btn_2">Let us Know About Your Illness</a>

                        </div>
                    </div>
                </div>
                <div class="col-lg-7">
                    <div class="banner_img">
                        <img src="{{url('public/assets/img/banner_img2.png')}}" alt="">
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- banner part start-->

    <!-- about us part start-->
    <section class="about_us padding_top">
        <div class="container">
            <div class="row justify-content-between align-items-center">
                <div class="col-md-6 col-lg-6">
                    <div class="about_us_img">
                        <img src="{{url('public/assets/img/top_service.png')}}" alt="">
                    </div>
                </div>
                <div class="col-md-6 col-lg-5">
                    <div class="about_us_text">
                        <h2>About us</h2>
                        <p>In December. 2019, a new coronavirus was discovered which causes a respiratory disease. This has been named COVID-19. The virus has now been declared a pandemic by the World Health Organization. As individuals and communities, we need to take action to prevent, isolate and care for ourselves and others during the outbreak.
This website will help everyone to face this world wide pandemic situation. This is one of the initiatives taken by ‘Babylon Resources Limited and Newgen Technology Limited’ to serve the society.
</p>
                        
                        <div class="banner_item">
                            <div class="single_item">
                                <img src="{{url('public/assets/img/icon/banner_1.svg')}}" alt="">
                                <h5>Emergency</h5>
                            </div>
                            <div class="single_item">
                                <img src="{{url('public/assets/img/icon/banner_2.svg')}}" alt="">
                                <h5>Appointment</h5>
                            </div>
                            <div class="single_item">
                                <img src="{{url('public/assets/img/icon/banner_3.svg')}}" alt="">
                                <h5>Qualfied</h5>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- about us part end-->

    <!-- feature_part start-->
    <section class="feature_part">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-xl-8">
                    <div class="section_tittle text-center">
                        <h2>Basic protective measures against the new Coronavirus</h2>
                    </div>
                </div>
            </div>
            <div class="row justify-content-between align-items-center">
                <div class="col-lg-3 col-sm-12">
                    <div class="single_feature">
                        <div class="single_feature_part">
                            <span class="single_feature_icon">
                                <img src="{{url('public/icons/icon_1.svg')}}" alt=""/></span>
                            <h4>Wash your hands frequently</h4>
                            <p>Wash your hands frequently and Maintain social distancingWashing your hands with soap and water or using alcohol-based hand rub kills viruses that may be on your hands.</p>
                        </div>
                    </div>
                    <div class="single_feature">
                        <div class="single_feature_part">
                            <span class="single_feature_icon"><img src="{{url('public/icons/icon_2.svg')}}" alt=""></span>
                            <h4>Don't touch eyes, nose and mouth</h4>
                            <p>Make sure you, and the people around you, follow good respiratory hygiene. This means covering your mouth and nose with your bent elbow or tissue when you cough or sneeze. Then dispose of the used tissue immediately.</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-sm-12">
                        <div class="single_feature_img">
                            <img src="{{url('public/assets/img/service2.png')}}" alt="">
                        </div>
                </div>
                <div class="col-lg-3 col-sm-12">
                    <div class="single_feature">
                        <div class="single_feature_part">
                            <span class="single_feature_icon"><img src="{{url('public/icons/icon_3.svg')}}" alt=""></span>
                            <h4>Seek medical care early</h4>
                            <p>Stay home if you feel unwell. If you have a fever, cough and difficulty breathing, seek medical attention and call in advance. Follow the directions of your local health authority. </p>
                        </div>
                    </div>
                    <div class="single_feature">
                        <div class="single_feature_part">
                            <span class="single_feature_icon"><img src="{{url('public/icons/icon_4.svg')}}" alt=""></span>
                            <h4>Follow advice of healthcare provider</h4>
                            <p>Stay informed on the latest developments about COVID-19. Follow advice given by your healthcare provider, your national and local public health authority or your employer on how to protect yourself and others from COVID-19.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- feature_part start-->


    <!--::regervation_part start::-->
    <section class="regervation_part section_padding">
        <div class="container">
            <div class="row align-items-center regervation_content">
                <div class="col-lg-12">
                    <h1 class="big-title">Stay Home, Stay Safe!</h1>
                </div>

            </div>
        </div>
    </section>
    <!--::regervation_part end::-->


    <!-- footer part start-->
@include('landing_footer')

