<?php 
	use App\Http\Controllers\HomeController;
	$obj = new HomeController;
	$auth = $obj->userInfo();
	if(is_null($auth))
	{
		return redirect()->route('login')->with('fail','Invalid role, pls login.');
	}
?>
			<!-- Sidebar content -->
			<div class="sidebar-content">

				<!-- User menu -->
				<div class="sidebar-user">
					<div class="card-body">
						<div class="media">
							<div class="mr-3">
								@if(empty(Auth::user()->image))
				                  <img src="{{url('public/images/avatar.png')}}" class="rounded-circle avatar" style="width:38px;height: 38px">
				                @else
				                  <img src="{{url('storage/app/avatars')}}/{{Auth::user()->image}}"class="rounded-circle avatar" style="width:38px;height: 38px"> 
				                @endif  
							</div>

							<div class="media-body">
								<div class="media-title font-weight-semibold">{{Auth::user()->name}}</div>
								<div class="font-size-xs opacity-50">
								{{$auth->email}}
								</div>
							</div>
							<!--
							<div class="ml-3 align-self-center">
								<a href="#" class="text-white"><i class="icon-cog3"></i></a>
							</div>
							-->
						</div>
					</div>
				</div>
				<!-- /user menu -->


				<!-- Main navigation -->
				<div class="card card-sidebar-mobile">
					<ul class="nav nav-sidebar" data-nav-type="accordion">

						<!-- Main -->

						<li class="nav-item-header"><div class="text-uppercase font-size-xs line-height-xs">Main</div> <i class="icon-menu" title="Main"></i></li>
						@if($auth->role==="ROLE_ADMIN")
						<li class="nav-item">
							<a href="{{route('home')}}" class="nav-link">
								<i class="icon-home4"></i>
								<span>
									Dashboard
								</span>
							</a>
						</li>													
						<li class="nav-item nav-item-submenu">
							<a href="#" class="nav-link"><i class="far fa-comments"></i> <span>Patients</span></a>

							<ul class="nav nav-group-sub" data-submenu-title="">
								
								<li class="nav-item">
									<a href="{{route('createPatient')}}" class="nav-link active">
										Create
									</a>
								</li>						
								<li class="nav-item">
									<a href="{{route('managePatinets',$page=0)}}" class="nav-link active">
										Manage
									</a>
								</li>
								
							</ul>
						</li>
						<li class="nav-item nav-item-submenu">
							<a href="#" class="nav-link"><i class="far fa-comments"></i> <span>Facilities</span></a>

							<ul class="nav nav-group-sub" data-submenu-title="Client Feedbacks">
								
								<li class="nav-item">
									<a href="{{route('createFacility')}}" class="nav-link active">
										Create
									</a>
								</li>						
								<li class="nav-item">
									<a href="{{route('facilities',$page=0)}}" class="nav-link active">
										Manage
									</a>
								</li>
								
							</ul>
						</li>								
						<li class="nav-item">
							<a href="{{route('manageUsers',$page=0)}}" class="nav-link">
								<i class="fas fa-users"></i>
								<span>
									Users
								</span>
							</a>
						</li>						

						<li class="nav-item">
							<a href="{{route('manageAuthority',$page=0)}}" class="nav-link">
								<i class="fas fa-address-card"></i>
								<span>
									Authority Users
								</span>
							</a>
						</li>
						<li class="nav-item nav-item-submenu">
							<a href="#" class="nav-link"><i class="far fa-comments"></i> <span>Survey</span></a>

							<ul class="nav nav-group-sub" data-submenu-title="Client Feedbacks">
								
								<li class="nav-item">
									<a href="{{route('manageSurvey',$page=0)}}" class="nav-link active">
										New List
									</a>
								</li>						
								<li class="nav-item">
									<a href="{{route('surveyReadList',$page=0)}}" class="nav-link active">
										Follow up List
									</a>
								</li>
								
							</ul>
						</li>
						<li class="nav-item">
							<a href="{{route('manageSymptoms',$page=0)}}" class="nav-link">
								<i class="fas fa-user-md"></i>
								<span>
									Physical Symtomps
								</span>
							</a>
						</li>						

							

						<li class="nav-item">
							<a href="{{route('manageQuarantine',$page=0)}}" class="nav-link">
								<i class="fas fa-eye"></i>
								<span>
									Quarantine Monitoring
								</span>
							</a>
						</li>						
						@endif

						@if($auth->role==="ROLE_USER")
						<li class="nav-item nav-item-submenu">
							<a href="#" class="nav-link"><i class="far fa-comments"></i> <span>Patients</span></a>

							<ul class="nav nav-group-sub" data-submenu-title="">
								
								<li class="nav-item">
									<a href="{{route('createPatient')}}" class="nav-link active">
										Create
									</a>
								</li>						
								<li class="nav-item">
									<a href="{{route('managePatinets',$page=0)}}" class="nav-link active">
										Manage
									</a>
								</li>
								
							</ul>
						</li>
						@endif

						@if($auth->role==="ROLE_MONITOR")
						<li class="nav-item">
							<a href="{{route('manageQuarantine',$page=0)}}" class="nav-link">
								<i class="fas fa-eye"></i>
								<span>
									Quarantine Monitoring
								</span>
							</a>
						</li>						
						@endif
					</ul>
				</div>
				<!-- /main navigation -->

			</div>
			<!-- /sidebar content -->