@include('landing_header')
    <!-- Header part end-->

    <!-- banner part start-->
    <section class="banner_part">
        <div class="container form-wrapper">
            <div class="row align-items-center">
                <div class="col-md-12">
                    <h1>Let us know about your illness</h1>
                    <span style="color:red">* marked fields are required</span>
                    @include('elements.errors')
                    <form class="surveyForm" method="post" action="{{route('surveyPost')}}" style="margin-top: 50px;">
                        @csrf
                        <div class="form-group row">
                            <div class="col-md-4">
                                <label>Full Name*</label>
                                <input type="" name="fullName" class="form-control" required value="{{ old('fullName') }}">
                            </div>                            
                            <div class="col-md-4">
                                <label>Father's Name*</label>
                                <input type="" name="fatherName" class="form-control"  required value="{{old('fatherName')}}">
                            </div>                            
                            <div class="col-md-4">
                                <label>Mother's Name*</label>
                                <input type="" name="motherName" class="form-control"  required value="{{old('motherName')}}">
                            </div>       
                        </div>                        

                        <label class="label"><strong>NID OR BIRTH CERTIFICATE HAVE TO FILL</strong></label>
                        <div class="form-group row">
                            <div class="col-md-6">
                                <label>National ID No:*</label>
                                <input type="number" name="nidNo" class="form-control" value="{{old('nidNo')}}">
                            </div>                            
                            <div class="col-md-6 no-p">
                                <label>Birth Certificate No:</label>
                                <input type="number" name="birthCertificateNo" class="form-control" value="{{old('birthCertificateNo')}}">
                            </div>                            

                        </div>                          
                        <label class="label"><strong>PRESENT ADDRESS*</strong></label>
                        <div class="form-group row">
                            <div class="col-md-4">
                                <label>Village*</label>
                                <input type="" name="village" class="form-control" required value="{{old('village')}}">
                            </div>                            
                            <div class="col-md-4 no-p">
                                <label>Post Office*</label>
                                <input type="" name="postOffice" class="form-control"  required value="{{old('postOffice')}}" required>
                            </div>                              
                            <div class="col-md-4 no-p">
                                <label>Post Code</label>
                                <input type="number" name="postCode" class="form-control"  value="{{old('postCode')}}">
                            </div>                             
                            <div class="col-md-4">
                                <label>Upazila*</label>
                                <input type="" name="upazila" class="form-control"  required value="{{old('upazila')}}">
                            </div>
                            <div class="col-md-4 no-p">
                                <label>District*</label>
                                <input type="" name="district" class="form-control preDis"   required value="{{old('district')}}">
                            </div>                                  
                            <div class="col-md-4 no-p">
                                <label>Division*</label>
                                <select class="form-control preDiv" name="division" required>
                                    <option value="">Select</option>
                                    @forelse($divisions as $d)
                                    <option value="{{$d->name}}" id="{{$d->id}}">{{$d->name}}</option>
                                    @empty
                                    <option>No record found</option>
                                    @endforelse
                                </select>
                            </div>                                                   
                              
                        </div>   
                        <input type="checkbox" id="same" name="same">
                        <label for="same">Same as Present Address</label>
                        <br>
                        <label class="label">
                            <strong>PERMANENT ADDRESS*</strong>
                        </label>
                        <div class="form-group row">
                            <div class="col-md-4">
                                <label>Village*</label>
                                <input type="" name="per_village" id="village" class="form-control" required value="{{old('village')}}">
                            </div>                            
                            <div class="col-md-4 no-p">
                                <label>Post Office*</label>
                                <input type="" name="per_postOffice" class="form-control" id="postOffice" value="{{old('postOffice')}}" required>
                            </div>                              
                            <div class="col-md-4 no-p">
                                <label>Post Code</label>
                                <input type="number" name="per_postCode" id="postCode" class="form-control"  value="{{old('postCode')}}">
                            </div>                             
                            <div class="col-md-4">
                                <label>Upazila*</label>
                                <input type="" id="upazila" name="per_upazila" class="form-control"  required value="{{old('upazila')}}">
                            </div>  
                            <div class="col-md-4 no-p">
                                <label>District*</label>
                                <input type="" id="district" name="per_district" class="form-control perDis"  required value="{{old('district')}}">
                            </div>                            
                            <div class="col-md-4 no-p">
                                <label>Division*</label>
                                <select class="form-control perDiv" name="per_division"  id='division' required>
                                    <option value="">Select</option>
                                    @forelse($divisions as $d)
                                    <option value="{{$d->name}}">{{$d->name}}</option>
                                    @empty
                                    <option>No record found</option>
                                    @endforelse
                                </select>
                            </div>                                                        
                                                         
                        </div>   

                        <div class="form-group row">
                            <div class="col-md-6">
                                <label>Mobile Number*</label>
                                <input type="number" name="mobileNumber" class="form-control" required value="{{old('mobileNumber')}}">
                            </div>                           
                            <div class="col-md-6 no-p">
                                <label>Date of Birth*(1990-01-01 yyyy-mm-dd)</label>
                                <input type="date" max="<?= date('Y-m-d',strtotime("-1 days"));?>" name="dateOfBirth" class="form-control" required value="{{old('dateOfBirth')}}">
                            </div>                            

                        </div>                         

                        <div class="form-group row">
                            <div class="col-md-4">
                                <label>Gender*</label>
                                <select class="form-control" name="gender" required>
                                    @if(!empty(old('gender')))
                                    <option value="{{old('gender')}}">{{old('gender')}}</option>
                                    @else
                                    <option value="">Select</option>
                                    @endif
                                    <option value="Male">Male</option>
                                    <option value="Female">Female</option>
                                    <option value="Other">Other</option>
                                </select>
                            </div>                          
  
                            <div class="col-md-4">
                                <label>Blood Group*</label>
                                <select class="form-control" name="bloodGroup" required>
                                    <option value="A+">A+</option>
                                    <option value="A-">A-</option>
                                    <option value="B+">B+</option>
                                    <option value="B-">B-</option>
                                    <option value="AB+">AB+</option>
                                    <option value="AB-">AB-</option>
                                    <option value="O+">O+</option>
                                    <option value="O-">O-</option>
                                </select>
                            </div>                              
                            <div class="col-md-4">
                                <label>Number of Days Sick*</label>
                                <input type="number" name="numOfDaysSick" class="form-control"  required value="{{old('numOfDaysSick')}}">
                            </div>                            

                        </div> 

                        <div class="form-group row">
                            <div class="col-md-6">
                                <label>Number Of Contacted Person*</label>
                                <input type="number" class="form-control" name="numOfContInftPerson" required value="{{old('numOfContInftPerson')}}">
                            </div>    

                            <div class="col-md-6">
                                <label>Meet with Foreigners / Suspected Person</label>
                                <div class="form-check">
                                  <input class="form-check-input" type="radio" name="isForeignVisitor" id="yes" value="true" required>
                                  <label class="form-check-label" for="yes">
                                    Yes
                                  </label>
                                </div>                               
                                 <div class="form-check">
                                  <input class="form-check-input" type="radio" name="isForeignVisitor" id="no" value="false" required>
                                  <label class="form-check-label" for="no">
                                    No
                                  </label>
                                </div>
                            </div>                                                     
             
                        </div>                        


                        <div class="form-group row">                               
                            <div class="col-md-12">
                                <label>Physical Symptoms</label>
                                <br>

                                @forelse($symptoms as $s)
                                    <div class="form-check form-check-inline col-md-10">
                                      <input class="form-check-input" type="checkbox" id="{{$s->name}}" value="{{$s->id}}" name="physicalSymptoms[]">
                                      <label class="form-check-label col-md-4" for="{{$s->name}}">{{$s->name}}</label>

                                      <input type="number" name="days[]" class="form-control col-md-4" min="0" placeholder="Number of days">
                                    </div>                                
                                @empty
                                
                                @endforelse
                            </div>   
                        </div>                                                  
                        <div class="form-group row">
                            <div class="col-md-12">
                                <label>Remarks</label>
                                <textarea class="form-control" name="remarks" >{{old('remarks')}}</textarea>
                            </div>   
                        </div>
                        
                        <div class="form-group row">
                            <div class="col-md-12">
                                <button class="btn btn-primary btn-lg">Submit Information</button>
                            </div>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </section>
    <!-- banner part start-->


    <!-- footer part start-->

@include('landing_footer')
<script type="text/javascript">
    $(document).ready(function(){

        $('input[name="same"]').click(function(){

            if($(this).is(":checked")){
                let village = $('[name="village"]').val();
                let dis = $('[name="district"]').val();
                let div = $('[name="division"]').val();
                let upazila = $('[name="upazila"]').val();
                let poffice = $('[name="postOffice"]').val();
                let pocode = $('[name="postCode"]').val();                

                $('#village').val(village);
                $('#district').val(dis);
                $('#division').prepend(`<option value="${div}" selected>${div}</option>`);
                $('#upazila').val(upazila);
                $('#postOffice').val(poffice);
                $('#postCode').val(pocode);
            }

            else if($(this).is(":not(:checked)")){
                $('#village').val('');
                $('#district').val('');
                $('#division').val('');
                $('#upazila').val('');
                $('#postOffice').val('');
                $('#postCode').val('');

            }

        });

        //district as per div
        /*
        $(".preDiv").on('change',function(){
            let divid = $(this).attr('id');
            getDistrict(divid)
        })
        */
    });
/*
        function getDistrict(divid)
        {
            alert(divid);
        }
*/
</script>