
    <!-- footer part start-->

    <footer class="footer-area">

        <div class="footer section_padding">
            <div class="container">
                <div class="row justify-content-between">
                    <div class="footer-content">

                      
                            <?php 
                            $IEDCR = ['01550064902','01550064903', '01550064904', '01550064905', '01401184551', '01401184554', '01401184555', '01401184556', '01401184559', '01401184560', '01401184563', '01401184568', '01927711784', '01927711785', '01937000011', '01937110011'];
                            ?>

                        <div class="card">
                            <div class="card-header">
                                <h2>IEDCR help line numbers</h2>
                            </div>
                            <div class="card-body text-center">
                                @forelse($IEDCR as $i)
                                <span class="badge badge-pill badge-primary" style="font-size: 16px;margin:5px;">{{$i}}</span>
                                @empty
                                @endforelse                   
                                <div class="icons">
                                <a href="https://www.iedcr.gov.bd/" target="_blank"><i class="fas fa-globe-asia"></i></a>        
                                <a href="https://www.facebook.com/iedcr" target="_blank"><i class="fab fa-facebook"></i></a>
                                
                                <a href="https://mail.google.com/mail/u/0/?view=cm&fs=1&to=iedcrit@gmail.com&tf=1" target="_blank"><i class="far fa-envelope"></i></a>                                    
                                </div>        
                            </div>
                        </div>                        

                        <div class="card">
                            <div class="card-header">
                                <h2>Hunting number</h2>
                            </div>
                            <div class="card-body text-center">
                               
                                <span class="badge badge-pill badge-primary" style="font-size: 16px;margin:5px;">01944333222</span>
                                 
                            </div>
                        </div>                        

                        <div class="card">
                            <div class="card-header">
                                <h2>a2i help line number</h2>
                            </div>
                            <div class="card-body">
                               
                                <span class="badge badge-pill badge-primary" style="font-size: 16px;margin:5px;">333</span>
                                 
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>

        <div class="copyright_part">
            <div class="container">
                <div class="row align-items-center">
                    <p class="footer-text m-0 col-lg-8 col-md-12"><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | Developed by <a href="https://brlbd.com" target="_blank">Babylon Resouces Ltd</a>

</p>            
                </div>
            </div>
        </div>
    </footer>

    <!-- footer part end-->

    <!-- jquery plugins here-->

    <script src="{{url('public/assets/js/jquery-1.12.1.min.js')}}"></script>

    <!-- popper js -->
    <script src="{{url('public/assets/js/popper.min.js')}}"></script>
    <!-- bootstrap js -->
    <script src="{{url('public/assets/js/bootstrap.min.js')}}"></script>
    <script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0"></script>

    <script type="text/javascript">
var ctx = document.getElementById('myChart').getContext('2d');
const url = "<?php echo url('/get-summerize')?>";

let response = fetch(url);
response.then(res => res.json()).then((res)=> {
    if(res.status=='ok')
    {
        let data = res.msg;
        let markup = 
        `
        <tr><td><i class="fas fa-circle" style="color:#778899"></i> Home Quarantine</td><td>${data.home_quarantine}</td></tr>
        <tr><td><i class="fas fa-circle" style="color:#FFFF01"></i> Quarantine</td><td>${data.quarantine}</td></tr>
        <tr><td><i class="fas fa-circle" style="color:#F8CE48"></i> Isolation</td><td>${data.isolation}</td></tr>
        <tr><td><i class="fas fa-circle" style="color:#EB702D"></i> Affected</td><td>${data.affected}</td></tr>
        <tr><td><i class="fas fa-circle" style="color:#34A853"></i> Recovered</td><td>${data.recovered}</td></tr>
        <tr><td><i class="fas fa-circle" style="color:#EA4335"></i> Dead</td><td>${data.dead}</td></tr>
        `;
        $("#livetable").html(markup);
//pie start    
        var myPieChart = new Chart(ctx, {
            type: 'pie',
            data:{
                datasets: [{
                    data: [data.home_quarantine, data.quarantine, data.isolation,data.affected,data.recovered,data.dead],
                    backgroundColor: [
                        "#778899",
                        "#FFFF01",
                        "#F8CE48",
                        "#EB702D",
                        "#34A853",
                        "#EA4335",

                  ],            
                }],

                // These labels appear in the legend and in the tooltips when hovering different arcs
                labels: [
                    'Home Quarantine '+( typeof data.home_quarantine  !== "undefined" ? data.home_quarantine :""),
                    'Quarantine '+( typeof data.quarantine  !== "undefined" ? data.quarantine :""),
                    'Isolation '+( typeof data.isolation  !== "undefined" ? data.isolation :""),
                    'Affected '+( typeof data.affected  !== "undefined" ? data.affected :""),
                    'Recovered '+ ( typeof data.recovered  !== "undefined" ? data.recovered :""),
                    'Dead '+( typeof data.dead  !== "undefined" ? data.dead :""),

                ],

            },
            
            options: {
                legend: {
                    display: false,
                    labels: {
                       
                        fontSize: 16,
                    }
                },
                tooltips: {
                    mode: 'label',
                    callbacks: {
                        label: function(tooltipItem, data) { 
                            var indice = tooltipItem.index;                 
                            return  data.labels[indice] ;
                        }
                    }
                },
            }
        });    
//pie start end        
    }
})
.catch(error=> console.log(`Error: ${error}`));


</script>
    
</body>

</html>