@include('landing_header')
    <!-- Header part end-->


    <!-- banner part start-->
    <section class="banner_part">
        <div class="container form-wrapper">
            <div class="row align-items-center">
                <div class="col-md-6 offset-md-3">
                    <div class="card login-card">
                        <div class="card-header">Login {{Cookie::get('token')}}</div>

                        <div class="card-body">
                            @include('elements.errors')
                            <form class="form" method="post" action="{{route('loginPost')}}">
                            @csrf    
                                <div class="form-group">
                                    <label>User Name</label>
                                    <input type="text" name="username" class="form-control" required>
                                </div>                                
                                <div class="form-group">
                                    <label>Password</label>
                                    <input type="password" name="password" class="form-control" required>
                                </div> 
                                <div class="form-group">
                                    
                                    <input type="checkbox" name="remember" id="remember">
                                    <label for="remember">Remember Me</label>
                                </div>
                                
                                                        
                                <div class="form-group">
                                    
                                        <button class="btn btn-primary btn-block">Login</button>
                                    
                                </div>
                                <a href="{{route('forgotPassword')}}">Forgot Password?</a>
                            </form>                            
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>
    <!-- banner part start-->



@include('landing_footer')
