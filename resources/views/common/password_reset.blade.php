@include('landing_header')
    <!-- Header part end-->


    <!-- banner part start-->
    <section class="banner_part">
        <div class="container form-wrapper">
            <div class="row align-items-center">
                <div class="col-md-6 offset-md-3">
                    <div class="card login-card">
                        <div class="card-header">Forgot Password</div>

                        <div class="card-body">
                            <form class="form" method="post" action="{{route('loginPost')}}">
                            @csrf    
                                <div class="form-group">
                                    <label>Email</label>
                                    <input type="email" name="email" class="form-control" required>
                                </div>                         
                                
                                                        
                                <div class="form-group">
                                    
                                        <button class="btn btn-primary btn-block">Send Password Rest Link</button>
                                    
                                </div>
                                
                            </form>                            
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>
    <!-- banner part start-->



@include('landing_footer')
