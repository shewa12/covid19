<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>medical</title>
    <link rel="icon" href="{{url('public/assets/img/favicon.png')}}">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{url('public/assets/css/bootstrap.min.css')}}">

    <!-- style CSS -->
    <link rel="stylesheet" href="{{url('public/assets/css/style.css')}}">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.1/css/all.css" integrity="sha384-O8whS3fhG2OnA5Kas0Y9l3cfpmYjapjI0E4theH4iuMD+pLhbf6JI0jIMfYcK3yZ" crossorigin="anonymous">
    <style type="text/css">
        body {
            background: #f1f9ff;
        }
        #app {
            height: 100vh;
        }
        .navbar-brand{
            -webkit-margin-right:300px !important;
        }
        /*over ride css*/
        .feature_part .single_feature_part span
        {
            width: 100% !important;
            height: 100% !important;
            background-color: transparent;
        }
        .feature_part .single_feature_part span img {
            width: 225px !important;
        }

        .nav-item {
            margin-left: 10px;
        }
        .big-title {
            font-size: 116px;
            font-weight: bold;
            color: #fff;
        }

        .form-wrapper {
            margin-top:120px;
           
        } 
        .footer-content {
            text-align: center;
        }    
        .section_tittle h2 {
            line-height: 50px;
        }  
        .banner_part .banner_text{
            height: auto !important;
            margin-top:200px;
        }
        .login-card
        {
            margin-bottom: 50px;
        } 
        .icons {
            margin-top: 20px;
            font-size: 24px;
        }
        .icons a {
            padding-right:10px;
        }
        /*medium device*/
        @media only screen and (max-width:992px){
            .big-title {
                font-size: 76px !important;
            }
            .banner_part, .banner_text{
                height: auto !important;
            }  
      
        }
        /*small device*/
        @media only screen and (max-width: 768px)
        {
            .single_feature_part p {
                margin-top:30px ;
            }
        }
    </style>
    <!--google translator-->
    <script type="text/javascript">
    function googleTranslateElementInit() {
      new google.translate.TranslateElement({pageLanguage: 'en'}, 'google_translate_element');
    }
    </script>

    <script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>    
</head>

<body>
    <!--::header part start::-->
    <header class="main_menu home_menu">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-12">
                    <nav class="navbar navbar-expand-lg navbar-light">

                   
                        <a class="navbar-brand col-md-3" href="{{url('')}}"> <img src="{{url('public/images/logo1.png')}}" alt="COVID-19"> </a>
                        
                        <button class="navbar-toggler" type="button" data-toggle="collapse"
                            data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                            aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-icon"></span>
                        </button>

                        <div class="collapse navbar-collapse main-menu-item"
                            id="navbarSupportedContent">
                            <ul class="nav navbar-nav align-items-center">
                                <li class="nav-item active">
                                    <a class="nav-link" href="{{url('')}}">Home </a>
                                </li>                         @if(Auth::user())       
                                <li class="nav-item active">
                                    <a class="nav-link" href="{{route('home')}}">Dashboard</a>
                                </li>
                                @else
                                <li class="nav-item active">
                                    <a class="nav-link" href="{{route('login')}}">Login</a>
                                </li>
                                @endif

                                <li class="nav-item">
                                    <a class="btn_2" href="{{route('surveyForm')}}">Feeling Sick</a>
                                </li>
         
                                <li class="nav-item">
                                    <a class="btn_2" href="#">DGHS: 16263</a>
                                </li>                                
                                <li class="nav-item" id="google_translate_element">
                                    
                                </li>
                            </ul>
                        </div>
                        
                    </nav>
                </div>
            </div>
        </div>
    </header>