<?php 
namespace App\Http\Controllers;


trait ResponseHandlerTrait
{


    //post username, pass
    function handleResponse($response,$sMsg)
    {
        $body = json_decode($response->body());
        if($response->successFul())
        {
            return redirect()->back()->with('success',$sMsg);
        }
        else if($response->status()===401)
        {
            $this->loginObj->delete();
            return redirect()->route('login');
        }
        else if ($response->serverError())
        {
            return redirect()->back()->withInput()->with('fail','Server error, pls try again.');
        }
        else
        {
            return redirect()->back()->withInput()->with('fail',$body->error);
        }
    }     

    function updateResponse($response)
    {
    
        $body = json_decode($response->body());
        if($response->successFul() AND $body->status=="validationError")
        {
            return redirect()->back()->with('fail',$body->messgae);
        }
        else if($response->successFul() AND $body->status =="error")
        {
            return redirect()->back()->with('fail',"Internal error, pls try again."); 
        }       
        else if($response->successFul() AND $body->status =="ok")
        {
            return redirect()->back()->with('success',$body->messgae); 
        }       
        else if($response->status() ==401)
        {
            return redirect()->route('login')->with('fail','Access token expired, pls login.');
        }
        else
        {
            return redirect()->back()->with('fail','Something went wrong, pls try again.');
        }        
    }

}
?>