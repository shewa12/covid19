<?php 
namespace App\Http\Controllers;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

trait UserTrait
{


    //post username, pass
    function apiPost($url,$post)
    {
        if(empty(Auth::user()->token))
        {   
            DB::table('users')->where('id',Auth::id())->delete();
            return redirect()->route('login')->with('fail','Access token as expired, pls login.');
        }

        $token = Auth::user()->token;        
    	$token = Auth::user()->token;
		$response = Http::withToken($token)->post($url,$post);
		//return $response;

		return $response;
    }     

    function apiPostWithoutToken($url,$post)
    {
        $response = Http::post($url,$post);
        //return $response;

        return $response;
    }    

    function apiGet($url)
    {   
        if(empty(Auth::user()->token))
        {   
            DB::table('users')->where('id',Auth::id())->delete();
            return redirect()->route('login')->with('fail','Access token as expired, pls login.');
        }

        $token = Auth::user()->token;
 
    	
		$response = Http::withToken($token)->get($url);
		//return $response;

		return $response;
    }    

    function apiGetWithoutToken($url)
    {
        
        $response = Http::get($url);
        //return $response;

        return $response;
    }

}
?>