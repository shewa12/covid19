<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SymptomController extends Controller
{
	use UserTrait;
	use ResponseHandlerTrait;
	private $apiurl;
	private $createUrl= "physicalSymptomInfo/save";
	private $getAllUrl= "physicalSymptomInfo/allList";
	private $editUrl= "physicalSymptomInfo/update";

    function __construct()
    {
    	$this->apiurl = env('APIURL');
    }

    function manageSymptoms($page)
    {
        $response = $this->getAll($page);  

	    return view('dashboard.symptoms')->with([
	            'title'=> 'Manage Symptoms',
	            'records'=> $response,
	        ]);           	
  	
    }

    function createSymptom(Request $request)
    {

    	$post = $request->except('_token');


    	if(isset($request->id))
    	{
    	
    		$response = $this->update($post);
    	}
    	else
    	{	

    		$response = $this->create($post);
    	}
    
    	return $this->updateResponse($response);
    }

    function getAll($page)
    {
        //get all
        $post = ['pageNumber'=> $page,'pageSize'=>100];
        $response = $this->apiPost($this->apiurl.$this->getAllUrl,$post);
        if($response->successFul())
        {
            return $body = json_decode($response->body());
            
        }
        else if($response->status()===401)
        {
            $this->loginObj->delete();
            return redirect()->route('login')->with('fail','Access token expired, pls login.');
        }
        else
        {
            return $body = [];
        }
    }

    function deleteSymptom($id)
    {
        $response = $this->delete($id);
    }

    function create($post)
    {
    	//create or update

    	$response = $this->apiPost($this->apiurl.$this->createUrl,$post);
    	return $response;
    }    

    function update($post)
    {
    	//create or update

    	$response = $this->apiPost($this->apiurl.$this->editUrl,$post);
    	return $response;
    }

    function delete($id)
    {

    }
}
