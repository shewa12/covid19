<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Auth\LoginController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\FacilityController;
use App\Http\Controllers\Auth\SurveyController;
use App\Http\Controllers\UserController;

class AuthorityController extends Controller
{
	use UserTrait;

	private $apiurl;
	private $createUrl = 'userRegister';//post
	private $getAllUrl = 'userAll';//post
	private $getUrl = 'user';//get url/param
	private $editUrl = 'userEdit';//post
    private $facObj;
    private $loginObj;
    private $surveyObj;
    private $userObj;

	function __construct()
	{
		$this->apiurl = env('APIURL');
        $this->facObj = new FacilityController;
        $this->loginObj = new LoginController;
        $this->surveyObj = new SurveyController;
        $this->userObj = new UserController;
	}	

    function manageUsers($page)
    {
        $records = $this->users($page);  
        //$facilities = $this->facObj->getFacAsReq(1000);//it will return 100 records

        return view('dashboard.authority_user')->with([
            'title'=> 'Manage Authority Users',
            'records'=> $records,
            //'facilities'=> $facilities,
            'divisions'=> $this->surveyObj->allDivision(),
            'prev'=> $page-1,
            'next'=> $page+1,
        ]);        
    	
    }


    function createAuthority(Request $request)
    {
    	//create and update
        $msg ;
     
    	$validate = $this->doValidation($request->all());
    	if($validate->fails())
    	{
    		return redirect()->back()->with('errors',$validate->errors());
    	}

        $presentAddress = [
            'village'=> $request->village,
            'postOffice'=> $request->postOffice,
            'postCode'=> $request->postCode,
            'upazila'=> $request->upazila,
            'thana'=> $request->thana,
            'district'=> $request->district,
            'division'=> $request->division
        ];
        $post = [
            'userType'=> 2,
            'facilityId'=> null,
            'username'=> $request->username,
            'email'=> $request->email,
            'password'=> $request->password,
            'activated'=> $request->activated,
            'address'=> $presentAddress
        ];


        if(isset($request->id))
        {
            $msg = "User Updated";
            $post['id'] = $request->id;
            $response = $this->userObj->update($post);
        }
        else
        {

            $msg = "User Created";

            $response = $this->userObj->create($post);
        }

        $body = $response->body();
   
    	if($response->successFul() AND $body =="User Register Successfully")
    	{
    		return redirect()->back()->with('success',$msg);
    	}          
        else if($response->successFul() AND $body =="User Edit Successfully")
        {
            return redirect()->back()->with('success',$msg);
        }      
    

        else if($response->successFul() AND $body =="User Already Exist")
        {
            return redirect()->back()->with('fail',"User mail already exist");
        }
        
        else if($response->status()==401)
        {
            $this->loginObj()->delete();
            return redirect()->route('login')->with('fail','Access token expired, please login.');
        }
    	else if($response->serverError())
    	{
    		return redirect()->back()->with('fail',"Server error, please try again.");
    	}
    	else
    	{
    		return redirect()->back()->with('fail',"Something went wrong, please try again.");
    	}    

    }

    function users($page)
    {
        //get all
        $post = ['pageNumber'=> $page,'pageSize'=>100];
        $response = $this->apiPost($this->apiurl.$this->getAllUrl,$post);

        if($response->successFul())
        {
            return $body = json_decode($response->body());
            
        }
        else if($response->status()===401)
        {
            $this->loginObj->delete();
            return redirect()->route('login');
        }
        else
        {
            return $body = [];
        }

    }

    function user($id)
    {
    	$response = $this->apiGet($this->apiurl.$this->getUrl.'/'.$id);
    	dd($response->body());
    }

    function doValidation($request)
    {
        if(isset($request['password']))
        {
            return Validator::make($request,[
                'username'=> ['required'],
                'email'=> ['required','email'],
                'password'=> ['required','min:6']
            ]);            
        }
        else
        {
            return Validator::make($request,[
                'username'=> ['required'],
                'email'=> ['required','email'],
                //'password'=> ['required','min:6']
            ]);              
        }

    }
}
