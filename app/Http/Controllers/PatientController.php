<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\Auth\SurveyController;

class PatientController extends Controller
{

	use UserTrait;
    use ResponseHandlerTrait;
	private $apiurl;
	private $createUrl = 'patient/create';//post
	private $getAllUrl = 'patient/patientAll';//post
	private $getUrl = 'patient/get';//get
	private $editUrl = 'patient/edit';//post
    private $historyUrl = 'patient/showPatientHistory';//post
    private $changeStatusUrl = 'patient/changePatientStatus';//post
    private $trackUrl = 'patient/getNotificationAllMarked';//post
    private $loginObj;
    

	function __construct()
	{
		$this->apiurl = env('APIURL');
        $this->loginObj = new LoginController;
	}
    function createPatient()
    {
        $surveyObj = new SurveyController;
    	return view('dashboard.create_patient')->with([
    		'title'=> 'Create Patient',
            'divisions'=> $surveyObj->allDivision(),
            'symptoms'=> $surveyObj->allSymptoms()
    	]);
    }

    function managePatinets($page)
    {
    	//get all
    	
    	$response = $this->patients($page);
       
        if($response->status()==401)
        {
            //if 401 delete user
            $this->loginObj->delete();
            return redirect()->route('login');
        }
        else if($response->successFul())
        {
            $body = json_decode($response->body());
            
        }
        else
        {
            $body = [];
        }

        return view('dashboard.patients')->with([
            'title'=> 'Manage Patients',
            'records'=> $body,
            'prev'=> $page-1,
            'next'=> $page+1,
        ]);

    }

    function postPatient(Request $request) 
    {
        $validate  = $this->doValidation($request); 
        

        if($validate->fails())
        {
            return redirect()->back()->withInput()->with('errors',$validate->errors());
        }

        if($request->nidNo=='' AND $request->birthCertificateNo=='')
        {
            return redirect()->back()->withInput()->with('fail','Nid or Birth certificate number have to fill');
        }        
    	$msg ;

		$date = $request->dateOfBirth;
		$newDate = date_create($date);
		$dob=  date_format($newDate,'Y-m-d');    	

    	$survey=[];
    	
    	//$survey['surveyDate'] = date('Y-m-d');
    	$presentAddress = [
    		'village'=> $request->village,
    		'postOffice'=> $request->postOffice,
    		'postCode'=> $request->postCode,
    		'upazila'=> $request->upazila,
    		'district'=> $request->district,
    		'division'=> $request->division
    	];       	

        $permanentAddress = [
            'village'=> $request->per_village,
            'postOffice'=> $request->per_postOffice,
            'postCode'=> $request->per_postCode,
            'upazila'=> $request->per_upazila,
            'district'=> $request->per_district,
            'division'=> $request->per_division
        ];
    	//$survey['id'] = null;
    	$survey['surveyId'] = null;
    	$survey['latitude'] = 23.810331;
    	$survey['longitude'] = 90.412521;
    	$survey['fullName']= $request->fullName;     	
     	
    	$survey['fatherName']= $request->fatherName;     	
    	$survey['motherName']= $request->motherName;
        $survey['birthCertificateNo']= $request->birthCertificateNo;
        $survey['nidNo']= $request->nidNo;


    	$survey['mobileNumber']= $request->mobileNumber;     	
    	$survey['dateOfBirth']= $dob;     	
    	$survey['gender']= $request->gender;     	
    	$survey['bloodGroup']= $request->bloodGroup;     	
    	$survey['numOfDaysSick']= $request->numOfDaysSick;

        $symptoms = array_filter($request->physicalSymptoms);       
        $days = array_filter($request->days);//remove empty
        $days = array_values($days);//reseting keys             	
        $map = $this->symptomsDasyMap($symptoms,$days);
        $survey['phySymMapList']= $map;

    	$survey['remarks']= $request->remarks;     	
    	$survey['recommendation']= 'Isolation';//$request->recommendation;     	
    	$survey['presentAddress']= $presentAddress;     	
    	$survey['permanentAddress']= $permanentAddress;   
        $survey['meetForeigner']= $request->isForeignVisitor;

    	if(isset($request->id))
    	{
            $survey['id'] = $request->id;
            
    		$msg = "Patient updated.";
    		$response = $this->update($survey);
    	}
    	else 
    	{
            $survey['id'] = null;
            $survey['status']= $request->status;
    		$msg = "Patient created.";

    		$response = $this->create($survey);    		
    	}

        //NID Existed Register Successfully

        $body = json_decode($response->body());

        if($response->successFul() AND $body->status ==='validationError')
        {
            return redirect()->back()->withInput()->with('fail',$body->messgae);
        }        
        
        else if($response->serverError())
        {
            return redirect()->back()->withInput()->with('fail',"Server error, please try again");
        }        

        else if($response->successFul() AND $body->status =="ok")
        {
            return redirect()->back()->with('success',$msg);
        }        
        else if($response->status()==401)
        {
            return redirect()->route('login')->with('fail',"Access token expired, pls login.");
        }
        else
        {
            return redirect()->back()->withInput()->with('fail',"Something went wrong, please try again.");
        }
    }

    function editPatient($id)
    {
        $surveyObj = new SurveyController;
        $url = $this->apiurl.$this->getUrl.'/'.$id;
        $response = $this->apiGet($url);
        $body = json_decode($response->body());
        

     
        if($response->successFul())
        {
            //dd($body);

            return view('dashboard.edit_patient')->with([
                'title'=> 'Edit Patient',
                'records'=> $body,
                'divisions'=> $surveyObj->allDivision(),
                'symptoms'=> $surveyObj->allSymptoms()
            ]);            
        }
        else if($response->status()==401)
        {
            $this->loginObj->delete();
            return redirect()->route('login')->with('fail','Access token expired, please login.');            
        }   
        else if ($response->serverError())
        {
            return redirect()->back()->withInput()->with('fail','Server error, pls try again.');
        }
        else
        {
            return redirect()->back()->with('fail',$body->error);
        }
    }

    function changePatientStatus(Request $request)
    {
        $sMsg = "Patient status changed";
        $post = $request->except('_token');
        $response = $this->apiPost($this->apiurl.$this->changeStatusUrl,$post);
        return $this->handleResponse($response,$sMsg);
    }    

    function patientHistory($id)
    {
        $url = $this->apiurl.$this->historyUrl.'/'.$id;
        $response = $this->apiGet($url);
        $body = json_decode($response->body());

        if($response->successFul())
        {
            return view('dashboard.patient_history')->with([
                'title'=> 'Patient History',
                'records'=> $body
            ]);          
        }
        else if($response->status()==401)
        {
            $this->loginObj->delete();
            return redirect()->route('login')->with('fail','Access token expired, please login.');            
        }   
        else if ($response->serverError())
        {
            return redirect()->back()->withInput()->with('fail','Server error, pls try again.');
        }
        else
        {
            return redirect()->back()->with('fail',$body->error);
        }
    }

    function trackPatient($id)
    {
        $pageObj = ['pageNumber'=>0,'pageSize'=>100];
        $post=['patientId'=>$id,'marked'=>false,'pageObject'=>$pageObj];

        $response = $this->apiPost($this->apiurl.$this->trackUrl,$post);
        if($response->status()==401)
        {
            return redirect()->back()->with('fail','Access token expired, pls login.');
        }
        else if($response->successFul())
        {
            $body = json_decode($response->body());
        }
        else 
        {
            $body = [];
        }
  
        return view('dashboard.patient_track')->with([
            'title'=> 'Track Patient',
            'records'=> $body,

        ]);        
    }

    function symptomsDasyMap($s,$d)
    {
        $arr = [];
        foreach ($s as $key=> $value) {
            if(isset($d[$key]))
            {
                $arr[] = [
                    'physicalSymptomInfoId'=> $value,
                    'numOfDaysSick'=> $d[$key]
                ];                
            }            
            else
            {
                $arr[] = [
                    'physicalSymptomInfoId'=> $value,
                    'numOfDaysSick'=> 0
                ];                
            }

        }

        return $arr;
    }
    //takes $post returns response
    function create($post)
    {
    	//create or update
    	$response = $this->apiPost($this->apiurl.$this->createUrl,$post);
    	return $response;
    }    

    function update($post)
    {
    	//create or update
    	$response = $this->apiPost($this->apiurl.$this->editUrl,$post);
    	return $response;
    }

    function patient($id)
    {
    	//get single patient
    	$url = $this->apiurl.$this->getUrl.'/'.$id;
    	$response = $this->apiGet($url);
    	return $response;    	
    }

    function patients($page)
    {
    	//get all patients
    	$post = ['pageNumber'=> $page, 'pageSize'=>100];
    	$response = $this->apiPost($this->apiurl.$this->getAllUrl,$post);
    	return $response;      	
    }
    function doValidation($request)
    {

        return Validator::make($request->all(),[
            'fullName'=> ['required','regex:/^[\pL\s\-]+$/u'],
            'dateOfBirth'=> ['required','date'],
            'physicalSymptoms'=> ['required'],
            'fatherName'=> ['required','regex:/^[\pL\s\-]+$/u'],
            'motherName'=> ['required','regex:/^[\pL\s\-]+$/u'],
            //'nidNo'=> ['integer','digits:13','min:0'],
            //'birthCertificateNo'=> ['numeric','digits:17','min:0'],
            'mobileNumber'=> ['required','numeric','digits:11'],
    
            'numOfDaysSick'=> ['required','numeric','min:1'],
            //'numOfContInftPerson'=> ['required','numeric','min:1'],

        ]);

    }    
}
