<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Auth\LoginController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\FacilityController;

class UserController extends Controller
{
	use UserTrait;

	private $apiurl;
	private $createUrl = 'userRegister';//post
	private $getAllUrl = 'userAll';//post
	private $getUrl = 'user';//get url/param
	private $editUrl = 'userEdit';//post
    private $facObj;
    private $loginObj;
    
	function __construct()
	{
		$this->apiurl = env('APIURL');
        $this->facObj = new FacilityController;
        $this->loginObj = new LoginController;
	}	

    function manageUsers($page)
    {
        $records = $this->users($page);  
        $facilities = $this->facObj->getFacAsReq(1000);//it will return 100 records

        return view('dashboard.user')->with([
            'title'=> 'Manage Users',
            'records'=> $records,
            'facilities'=> $facilities,
            'prev'=> $page-1,
            'next'=> $page+1,
        ]);        
    	
    }

    function createUser(Request $request)
    {
    	//create and update
        $msg ;

    	$validate = $this->doValidation($request->all());
    	if($validate->fails())
    	{
    		return redirect()->back()->with('errors',$validate->errors());
    	}

        if(isset($request->id))
        {
            $msg = "User Updated";
            $post = $request->except('_token');
            $post ['address'] = null;
            $post['userType'] = 1;
            $response = $this->update($post);
        }
        else
        {
            $msg = "User Created";
            $post = $request->except('_token');
            $post['userType'] = 1;
            $post ['address'] = null;
            $response = $this->create($post);
        }

        $body = $response->body();
   
    	if($response->successFul() AND $body =="User Register Successfully")
    	{
    		return redirect()->back()->with('success',$msg);
    	}          
        else if($response->successFul() AND $body =="User Edit Successfully")
        {
            return redirect()->back()->with('success',$msg);
        }      
    

        else if($response->successFul() AND $body =="User Already Exist")
        {
            return redirect()->back()->with('fail',"User mail already exist");
        }
        
        else if($response->status()==401)
        {
            $this->loginObj()->delete();
            return redirect()->route('login')->with('fail','Access token expired, please login.');
        }
    	else if($response->serverError())
    	{
    		return redirect()->back()->with('fail',"Server error, please try again.");
    	}
    	else
    	{
    		return redirect()->back()->with('fail',"Something went wrong, please try again.");
    	}    

    }

    function users($page)
    {
        //get all
        $post = ['pageNumber'=> $page,'pageSize'=>100];
        $response = $this->apiPost($this->apiurl.$this->getAllUrl,$post);
        if($response->successFul())
        {
            return $body = json_decode($response->body());
            
        }
        else if($response->status()===401)
        {
            $this->loginObj->delete();
            return redirect()->route('login');
        }
        else
        {
            return $body = [];
        }

    }

    function user($id)
    {
    	$response = $this->apiGet($this->apiurl.$this->getUrl.'/'.$id);
    	dd($response->body());
    }

    function create($post)
    {
        //dd($post);
        return $response = $this->apiPost($this->apiurl.$this->createUrl,$post);
    }    

    function update($post)
    {
        //dd($post);
        return $response = $this->apiPost($this->apiurl.$this->editUrl,$post);
    }

    function doValidation($request)
    {
        if(isset($request['password']))
        {
            return Validator::make($request,[
                'facilityId'=> ['required'],
                'username'=> ['required'],
                'email'=> ['required','email'],
                'password'=> ['required','min:6']
            ]);            
        }
        else
        {
            return Validator::make($request,[
                'facilityId'=> ['required'],
                'username'=> ['required'],
                'email'=> ['required','email'],
                //'password'=> ['required','min:6']
            ]);              
        }

    }
}
