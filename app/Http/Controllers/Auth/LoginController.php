<?php

namespace App\Http\Controllers\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use App\User;
class LoginController extends Controller
{

    use   AuthTrait; 
    /**
     * Handle an authentication attempt.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return Response
     */
    private $apiurl;
    private $loginUrl = 'authenticate';

    function __construct()
    {
        $this->apiurl = env('APIURL');
        $this->middleware('guest');
    }

    function login()
    {
        Cookie::make('token','12345',60);
        return view('common.login')->with([
            'title' => "Login"
        ]);
    }
    public function authenticate(Request $request)
    {
        $validate = Validator::make($request->all(),[
            'username'=> ['required'],
            'password'=> ['required']
        ]);
        if($validate->fails())
        {
            return redirect()->back()->with('errors',$validate->errors());
        }
        //checking user exits in db or not
        $credentials= [
            'name'=> $request->username,
            'password'=> $request->password
        ];
        if($this->checkUser($request->username,$request->password))
        {
            //authenticating
            if (Auth::attempt($credentials, $request->remember)) {
                // Authentication passed...
                return redirect()->route('home');
            }
            else
            {
                return redirect()->back()->with('fail',"Somewenting went wrong");
            }            
        }
        else
        {
            $fields = ['username'=> $request->username,'password'=> $request->password];
            $res = $this->apiPost($this->apiurl.$this->loginUrl, $fields);

            $resBody = json_decode($res->body());

            if($res->successFul())
            {
                
                //save user to db
                $post = ['name'=>$request->username,'password'=>Hash::make($request->password) ,'token'=> $resBody->id_token];
                if($this->create($post))
                {
                    //$credentials = $request->only('username', 'password');
                  
                    if (Auth::attempt($credentials, $request->remember)) {
                        // Authentication passed...
                        return redirect()->route('home');
                    }
                    else
                    {
                        return redirect()->back()->with('fail',"Email or password is incorrect.");
                    }                    
                }
            }
            else if($res->serverError())
            {
                return redirect()->back()->with('fail','Server error, please try again.');
            }
            else
            {
                return redirect()->back()->with('fail','User name or password incorrect.');
            }
        }

    }

    function checkUser($username,$password)
    {
        $user = DB::table('users')
            ->where('name',$username)
            ->where('token','<>',NULL)
            ->get();
        if(count($user)>0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    function create($post)
    {
        $user = new User($post);
        if($user->save())
        {
            return true;
        }
        return false;
    }

    function delete()
    {
            $delete = DB::table('users')
            ->where('id',Auth::id())
            ->delete();    

            if($delete)
            {
                return true;
            }    
    }

    function forgotPassword()
    {
        return view('common.password_reset')->with([
            'title'=> 'Forgot Password'
        ]);
    }
}