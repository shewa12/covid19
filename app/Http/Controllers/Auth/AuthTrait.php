<?php 
namespace App\Http\Controllers\Auth;
use Illuminate\Support\Facades\Http;

trait AuthTrait
{

    //private $surveyCreate = "surveyInfo/save";

    function apiPost($url,$post)
    {
    	//$token = Auth::user()->token;
		$response = Http::post($url,$post);
		//return $response;

		return $response;
    } 
    //post username, pass
    function apiAuthPost($url,$post)
    {
		$response = Http::withHeaders([
		    'Content-Type' => 'application/json',
		    
		])->post($url,$post);
		//return $response;

		return $response;
    }
    //take arr or response
    function response($response)
    {
		if($response->successFul())
		{
			return $res= ['status'=>'ok','msg'=>json_decode($response->body())];
		}	    	
		else if($response->serverError())
		{
			return $res= ['status'=>'error','msg'=>'Server error, please try again.'];
		}
		else if($response->clientError())
		{
			return $res= ['status'=>'error','msg'=>'Unauthorized, bad request'];
		}

		else
		{
			return $res= ['status'=>'error','msg'=>'User or password wrong.'];
		}
    }
}
?>