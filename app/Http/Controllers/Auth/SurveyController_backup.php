<?php

namespace App\Http\Controllers\Auth;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\UserTrait;
use App\Http\Controllers\PatientController;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;

class SurveyController extends Controller
{
	//use AuthTrait;
	use UserTrait;
	private $apiurl;
	private $createUrl = 'surveyInfo/save';//post
    private $getAllUrl = 'surveyInfoAll';//post
    private $getAllReadUrl = 'surveyInfoAllRead';//post
    private $getUrl = 'surveyInfo';//post
    private $divUrl = 'utill/allDivision';//get
    private $disUrl = 'geo/allDistricts';//get
    private $divWiseDisUrl = 'utill/division/';//get id param
    private $markReadUrl = 'surveyInfoUpdate';//get id param
    private $summarizeUrl = 'geo/summarize';
    private $allSymptomsUrl = 'physicalSymptomInfo/allList';

	private $loginObj;
    private $patientObj;

	function __construct()
	{
		$this->apiurl = env('APIURL');
       	$this->loginObj = new LoginController; 
        $this->patientObj = new PatientController; 
	}
    //guest post
    function surveyForm()
    {
        
        
        return view('survey_form')->with([
            'title'=> 'Survey Form',
            'divisions'=> $this->allDivision(),
            'symptoms'=> $this->allSymptoms()
        ]);
    }

    function surveyPost(Request $request) 
    {
        $validate  = $this->doValidation($request);	
    	

    	if($validate->fails())
    	{
    		return redirect()->back()->withInput()->with('errors',$validate->errors());
    	}

        if($request->nidNo=='' AND $request->birthCertificateNo=='')
        {
            return redirect()->back()->withInput()->with('fail','Nid or Birth certificate number have to fill');
        }
        $date = $request->dateOfBirth;
        $newDate = date_create($date);
        $dob=  date_format($newDate,'Y-m-d');        
   
    	$survey=[];
    	$survey['status']= "Free";
    	$survey['surveyDate'] = date('Y-m-d');
    	$presentAddress = [
    		'village'=> $request->village,
    		'postOffice'=> $request->postOffice,
    		'postCode'=> $request->postCode,
    		'upazila'=> $request->upazila,
    		'district'=> $request->district,
    		'division'=> $request->division
    	];       	

    	$permanentAddress = [
    		'village'=> $request->per_village,
    		'postOffice'=> $request->per_postOffice,
    		'postCode'=> $request->per_postCode,
    		'upazila'=> $request->per_upazila,
    		'district'=> $request->per_district,
    		'division'=> $request->per_division
    	];

    	$survey['fullName']= $request->fullName;     	
    	$survey['isForeignVisitor']= $request->isForeignVisitor;
    	$survey['fatherName']= $request->fatherName;     	
    	$survey['motherName']= $request->motherName;     	
    	$survey['nidNo']= $request->nidNo;     	
    	$survey['birthCertificateNo']= $request->birthCertificateNo;     	
    	$survey['mobileNumber']= $request->mobileNumber;     	
    	$survey['dateOfBirth']= $dob;     	
    	$survey['gender']= $request->gender;     	
    	$survey['bloodGroup']= $request->bloodGroup;     	
    	$survey['numOfContInftPerson']= $request->numOfContInftPerson;     	
        $survey['numOfDaysSick']= $request->numOfDaysSick;      
    	$survey['physicalSymptoms']= implode(',',$request->physicalSymptoms);     	
    	$survey['remarks']= $request->remarks;     	
    	$survey['presentAddress']= $presentAddress;     	
    	$survey['permanentAddress']= $permanentAddress;   
        $survey['isRead']= false;   
       
    	$response = $this->saveSurvey($survey); 	
        $resBody = json_decode($response->body());

    	if($response->successFul() AND $resBody->status=="ok")
    	{
    		return redirect()->back()->with('success','Form submitted.');
    	}      
        else if($response->successFul() AND $resBody->status=="validationError")
        {
            return redirect()->back()->withInput()->with('fail',$resBody->messgae);
        }
    	else if($response->status()===401)
    	{
    		$this->loginObj->delete();
    		return redirect()->route('login');
    	}
    	else if ($response->serverError())
    	{
    		return redirect()->back()->withInput()->with('fail','Server error, pls try again.');
    	}
    	else
    	{
    		return redirect()->back()->withInput()->with('fail',"Something went wrong, please try again");
    	}
    }

    function allDivision()
    {
        $divisions = $this->apiGetWithoutToken($this->apiurl.$this->divUrl); 
        return json_decode($divisions);       
    }

    function allSymptoms()
    {
        $post = [
            "pageNumber"=>0,
            "pageSize"=>50
        ];
        $response = $this->apiPost($this->apiurl.$this->allSymptomsUrl,$post);
        if($response->successFul())
        {
            return json_decode($response->body());
        }
        else
        {
            return [];
        }
    }

    function doValidation($request)
    {

        return Validator::make($request->all(),[
            'fullName'=> ['required','regex:/^[\pL\s\-]+$/u'],
            'dateOfBirth'=> ['required','date'],
            'physicalSymptoms'=> ['required'],
            //'fatherName'=> ['required','regex:/^[\pL\s\-]+$/u'],
            //'motherName'=> ['required','regex:/^[\pL\s\-]+$/u'],
            'gender'=> ['required'],
            //'birthCertificateNo'=> ['numeric','digits:17','min:0'],
            'mobileNumber'=> ['required','numeric','digits:11'],
    
            //'numOfDaysSick'=> ['required','numeric','min:1'],
            'numOfContInftPerson'=> ['required','numeric','min:1'],

        ]);

    }
    //admin access

    function manageSurvey($page)
    {
        $pageSize = 100;
        $response = $this->getAllSurvey($page,$pageSize);
        if($response->status()==401)
        {
            //if 401 delete user
            $this->loginObj->delete();
            return redirect()->route('login');
        }
        else if($response->successFul())
        {
            $body = json_decode($response->body());

        }
        else
        {
            $body = [];
        }
     
        return view('dashboard.survey')->with([
            'title'=> 'Manage Survey',
            'records'=> $body,
            'prev'=> $page-1,
            'next'=> $page+1,
            'pageName'=>'new'
        ]);
    }   

    function surveyReadList($page)
    {
        $pageSize = 100;
        $response = $this->getAllReadSurvey($page,$pageSize);
        if($response->status()==401)
        {
            //if 401 delete user
            $this->loginObj->delete();
            return redirect()->route('login');
        }
        else if($response->successFul())
        {
            $body = json_decode($response->body());

        }
        else
        {
            $body = [];
        }
      
        return view('dashboard.survey')->with([
            'title'=> 'Survey Read List',
            'records'=> $body,
            'prev'=> $page-1,
            'next'=> $page+1,
            'pageName'=>'read'
        ]);
    }

    function surveyMarkRead($id)
    {
        $url = $this->apiurl.$this->markReadUrl.'/'.$id;
        $response = $this->apiGet($url);
        $body = json_decode($response->body());
        if($response->successFul() AND $body->status=='ok')
        {
            return redirect()->back()->with('success','Marked as read.');
        }        
        else if($response->status()== 401)
        {
            return redirect()->route('login')->with('fail','Access token expired, pls login.');
        }        
        else if($response->serverError())
        {
            return redirect()->route('login')->with('fail','Server error, pls try again.');
        }
        else
        {
            
            return redirect()->back()->with('fail','Failed to mark as read.');
        }
    }
    function surveyDetail($id)
    {
        $response = $this->getSurvey($id);
        if($response->status()==401)
        {
            //if 401 delete user
            $this->loginObj->delete();
            return redirect()->route('login');
        }
        else if($response->successFul())
        {
            $body = json_decode($response->body());
            
        }
        else
        {
            $body = [];
        }
        
        return view('dashboard.survey_detail')->with([
            'title'=> 'Manage Survey',
            'records'=> $body,

        ]);
    }

    function markAsPatient($id)
    {
        //taking survey id & creating patient
        $response = $this->getSurvey($id);
        if($response->status()==401)
        {
            //if 401 delete user
            $this->loginObj->delete();
            return redirect()->route('login');
        }
        else if($response->successFul())
        {
            $body = json_decode($response->body());
            $body = (array) $body;
            $body['surveyId'] = $body['id'];
            $body['id'] = null;
            $post = $body;
            $response = $this->patientObj->create($post);
            $resBody = json_decode($response->body());
            if($response->status()==401)
            {
                //if 401 delete user
                $this->loginObj->delete();
                return redirect()->route('login');
            }
            else if($response->successFul())
            {
                return redirect()->back()->with('success','Marked as patient.');
            }
            else if($response->serverError())
            {
                return redirect()->back()->with('fail','Server error, please try again.');
            }
            else 
            {
                return redirect()->back()->with('fail','Something went wrong, please try again.');
            }
        }
        else
        {
            return redirect()->back()->with('fail',$resBody->error);
        }
    }

    function saveSurvey($survey)
    {
    	$response = $this->apiPostWithoutToken($this->apiurl.$this->createUrl,$survey);
   
    	return $response;
    }
    //require id
    function getSurvey($id)
    {
        $url = $this->apiurl.$this->getUrl.'/'.$id;
        return $response = $this->apiGet($url);
    }

    function getAllSurvey($page,$pageSize)
    {
        $post = ['pageNumber'=> $page, 'pageSize'=> $pageSize];
        $response = $this->apiPost($this->apiurl.$this->getAllUrl,$post);
        return $response;
    }    

    function getAllReadSurvey($page,$pageSize)
    {
        $post = ['pageNumber'=> $page, 'pageSize'=> $pageSize];
        $response = $this->apiPost($this->apiurl.$this->getAllReadUrl,$post);
        return $response;
    }

    function getMapData()
    {

        $response = $this->apiGetWithoutToken($this->apiurl.$this->disUrl);
        
        if($response->successFul())
        {
            $obj = ['status'=>'ok','msg'=>$response->body()];
            return response()->json($obj);
        }
        else
        {
            $obj = ['status'=>'error','msg'=>$response->body()];
            return response()->json($obj);
        }
        //$this->apiGetWithoutToken()
    }
    function getSummerize()
    {
        $status = ['home_quarantine','quarantine','isolation','affected','recovered','dead','released'];
        $keys;
        
        $fetch = $this->apiGetWithoutToken($this->apiurl.$this->summarizeUrl);
        if($fetch->successFul())
        {
            $obj = json_decode($fetch->body());
            $arr = (array)$obj;
            $arr = array_change_key_case($arr);

            $response = [
                'status'=>'ok',
                'msg'=> $arr
            ];
            return response()->json($response);     
        }
        else
        {
            $response = [
                'status'=>'error',
                'msg'=> "failed"
            ];
            return response()->json($response);
        }
       
              
    }    
}

