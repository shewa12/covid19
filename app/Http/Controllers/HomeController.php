<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use App\Http\Controllers\ResponseHandlerTrait;
use App\Http\Controllers\Auth\LoginController;
use App\User;

class HomeController extends Controller
{
    use UserTrait;
    use ResponseHandlerTrait;
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    private $apiurl;
    private $summarizeUrl = 'geo/summarize';
    private $infoUrl = "user";
    private $updatePassUrl = "user/changePassword";
    private $loginObj ;

    public function __construct()
    {
        $this->middleware('auth');
        $this->apiurl = env('APIURL');
        $this->loginObj = new LoginController;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $auth = $this->userInfo();
       
        if(!is_null($auth))
        {
            if($auth->role === 'ROLE_ADMIN')
            {
                $title= "Dashboard";
                $response = $this->apiGetWithoutToken($this->apiurl.$this->summarizeUrl);
                if($response->successFul())
                {
                    $body = json_decode($response->body());
                }
                else
                {
                    $body = [];
                }

                $arr = (array)$body;

                return view('dashboard.index')->with([
                    'title'=>$title,
                    'body'=> array_change_key_case($arr)
                ]);
            }
            else if($auth->role === 'ROLE_USER')
            {
                return redirect()->route('managePatinets',0);
            }
            else if($auth->role === 'ROLE_MONITOR')
            {
                return redirect()->route('manageQuarantine',0);
            }
            else
            {
                //if unknown role / delete user and redirect to login
                $this->loginObj->delete();
                return redirect()->route('login');                
            }
        }
        else
        {
            //if user info false, delete user redirect to login
            $this->loginObj->delete();
            return redirect()->route('login');            
        }

    }

    function userInfo()
    {
        $response = $this->apiGet($this->apiurl.$this->infoUrl);
        if($response->successFul())
        {
            return json_decode($response->body());
            
        }
        else if($response->status()===401)
        {
            $this->loginObj->delete();
            return redirect()->route('login');
        }
        else
        {
            $this->loginObj->delete();
            return redirect()->route('login');
        }        
    }

    function myProfile()
    {
        $title="My Profile";
        return view('dashboard.my_profile')->with([
            'title'=>$title
        ]);        
    }

    function updateAvatar(Request $request)
    {

        $request->validate([
            'image' =>'required|image|mimes:jpeg,png,jpg|max:1000'
        ]);


        $image = $request->file('image');
        $imageName = $image->getClientOriginalName();

        $post= [
            'image'=>$imageName,
        ];
        $q= User::where('id',Auth::id())->update($post);
                    
        if($q){
            $image->move('storage/app/avatars/',$imageName);
            return redirect()->back()->with('success',"Profile photo udpated.");
        }
        else{
            return redirect()->back()->with('fail',"Profile photo udpate failed.");
        }
    }    

    function updatePassword(Request $request)
    {
        $this->validate($request, [
           
           'password' => 'required|min:5|confirmed',
           
            ]);
        $newpassword= $request->password;
        $auth = $this->userInfo();
        $post = [
            'id'=> $auth->id,
            'newPassword'=> $newpassword
        ];

        $response = $this->apiPost($this->apiurl.$this->updatePassUrl,$post);
        
        $update= User::where('id',Auth::id())
                ->update(['password'=>bcrypt($newpassword)]);  
        return $this->updateResponse($response);
    }



    function logout()
    {
        Auth::logout();
        return redirect()->route('login');
    }
    function createUser()
    {
        $user = ['name'=>'shewa','email'=>'shewa@gmail.com','password' => Hash::make('shewa12')];
        $user = new User ($user);

        if($user->save())
        {
            echo "ok";
        }
        else
        {
            echo "failed";
        }
    }
}
