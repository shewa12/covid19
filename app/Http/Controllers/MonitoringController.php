<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Auth\LoginController;
use Illuminate\Http\Request;

class MonitoringController extends Controller
{
	use UserTrait;

	private $apiurl;
    private $monitorUrl = "monitor/getPatientNotification";
	function __construct()
	{
		$this->apiurl = env('APIURL');

	}	

    function manage($page)
    {

        return view('dashboard.monitoring')->with([
            'title'=> 'Quarantine Monitoring',
            'records'=> $this->notifications($page),
            'prev'=> $page-1,
            'next'=> $page+1,
        ]);        
    	
    }

    function notifications($page)
    {
        //get all
        $post = ['pageNumber'=> $page,'pageSize'=>100];
        $response = $this->apiPost($this->apiurl.$this->monitorUrl,$post);
        if($response->successFul())
        {
            return $body = json_decode($response->body());
            
        }
        else if($response->status()===401)
        {
            $this->loginObj->delete();
            return redirect()->route('login')->with('fail','Access token expired, pls login.');
        }
        else
        {
            return $body = [];
        }      
    }


}
