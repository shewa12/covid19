<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\Auth\SurveyController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class FacilityController extends Controller
{
	use UserTrait;
	private $apiurl;
	private $createUrl = 'facilityRegister';//post
	private $getAllUrl = 'facilityAll';//post
	private $getUrl = 'facility';//get
	private $editUrl = 'facilityRegisterEdit';//post
    private $loginObj;
    private $surveyObj;

	function __construct()
	{
		$this->apiurl = env('APIURL');
        $this->loginObj = new LoginController;
        $this->surveyObj = new SurveyController;
	}

    function createFacility()
    {
    	return view('dashboard.create_facility')->with([
    		'title'=> 'Create Facility',
            'divisions'=> $this->surveyObj->allDivision()
    	]);
    }
    //create & update
    function postFacility(Request $request)
    {
        $validate = $this->doValidation($request);
        if($validate->fails())
        {
            return redirect()->back()->withInput()->with('errors',$validate->errors());
        }
        $msg;
    	$facility = [
    		'facilityName'=> $request->facilityName,
    		'numberOfBeds'=> $request->numberOfBeds,
    		'numOfIcu'=> $request->numOfIcu,
    		'numberOfDoctors'=> $request->numberOfDoctors,
    		'numberOfVolunteers'=> $request->numberOfVolunteers,
            'numOfNurse'=> $request->numOfNurse,
            'numOfVentilator'=> $request->numOfVentilator,
            'numOfVaccine'=> $request->numOfVaccine,
    		'helpLineNumber'=> $request->helpLineNumber,
    		'latitude'=> 23.749,
    		'longitude'=> 24.749
    	];
    	$address = [
    		'village'=> $request->village,
    		'postOffice'=> $request->postOffice,
    		'postCode'=> $request->postCode,
    		'upazila'=> $request->upazila,
    		'district'=> $request->district,
    		'division'=> $request->division
    	];

    	$facility['address'] = $address;
        
    	if(isset($request->id))
    	{
    		$facility['id']= $request->id;
            $response = $this->apiPost($this->apiurl.$this->editUrl,$facility);
            $msg = "Facility updated.";            
    	}

        else
        {
            $response = $this->apiPost($this->apiurl.$this->createUrl,$facility);
            $msg = "Facility created.";
        }
    	$body = $response->body();
    
    	if($response->successFul() AND $body =="Register Successfully")
    	{
    		return redirect()->back()->with('success',$msg);
    	}      
        else if($response->successFul() AND $body =="Update Successfully")
        {
            return redirect()->back()->with('success',$msg);
        }
        else if($response->successFul() AND $body == "Facility Name Already Exist")
        {
            return redirect()->back()->withInput()->with('fail',$body);
        }        
        else if($response->successFul() AND $body == "Facility HelpLine Already Exist")
        {
            return redirect()->back()->withInput()->with('fail',$body);
        }
        else if($response->status()==401)
        {
            $this->loginObj->delete();
            return redirect()->route('login')->with('fail','Access to');
        }
    	else if($response->serverError())
    	{
    		return redirect()->back()->withInput()->with('fail',"Server error, please try again.");
    	}
    	else
    	{
    		return redirect()->back()->with('fail',"Something went wrong, please try again.");
    	}
    }

    function facilities($page)
    {
    	//get all
        //api body
    	$post = [
            "numOfBeds"=>false,
            "numOfDoctors"=>false,
            "numOfIcu"=>false,
            "numOfVolunteer"=>false,
        ];
        $postPage = ['pageNumber'=>0,'pageSize'=>10];
        $post['page'] = $postPage;

    	$response = $this->apiPost($this->apiurl.$this->getAllUrl,$post);
   
        if($response->status()==401)
        {
            //if 401 delete user
            $this->loginObj->delete();
            return redirect()->route('login');
        }
    	else if($response->successFul())
    	{
    		$body = json_decode($response->body());
    		
    	}
    	else
    	{
    		$body = [];
    	}
    
        return view('dashboard.facilities')->with([
            'title'=> 'Manage Facilities',
            'records'=> $body,
            'prev'=> $page-1,
            'next'=> $page+1,
            'divisions'=> $this->surveyObj->allDivision()
        ]);
    }

    function getFacAsReq($pageSize)
    {
        $post = [
            "numOfBeds"=>false,
            "numOfDoctors"=>false,
            "numOfIcu"=>false,
            "numOfVolunteer"=>false,
        ];
        $postPage = ['pageNumber'=>0,'pageSize'=>100];
        $post['page'] = $postPage;

        $response = $this->apiPost($this->apiurl.$this->getAllUrl,$post);
        if($response->successFul())
        {
            return $body = json_decode($response->body());
            
        }
        else if($response->status()===401)
        {
            $this->loginObj->delete();
            return redirect()->route('login');
        }
        else
        {
            return $body = [];
        }
    }


    function facility()
    {
    	//get single
    }

    function doValidation($request)
    {
        return Validator::make($request->all(),[
            'facilityName'=> ['required','string'],
            'numberOfBeds'=> ['required','numeric','min:0'],
            'numOfIcu'=> ['required','numeric','min:0'],
            'numberOfDoctors'=> ['required','numeric','min:0'],
            'numberOfVolunteers'=> ['required','numeric','min:0'],
            'helpLineNumber'=> ['required'],
            'postCode'=> ['required','numeric','min:1'],
            'division'=> ['required']
        ]);       
    }

}
