


  //console.log(add+update+del);
  function resetForm(formId)
  {
    document.getElementById(formId).reset();
  }

  function ajaxPostWithoutLoad(url,formData,div)
  {
    $(div).removeClass('alert alert-danger');
    $(div).empty();
    $.ajax({
      url: url,
      type: 'post',
      data: formData,
      dataType: 'json',
      beforeSend:()=>{
        $(".overlay").show();
      },
      success:(data)=>{
        $(".overlay").hide();
        if(data.status==="ok")
        {
          $.confirm({
              title: 'Success!',
              content: data.msg,
              buttons: {
                  ok: function () {
                      $("#addForm").reset();
                  },
                  /*
                  cancel: function () {
                      $.alert('Canceled!');
                  },

                  somethingElse: {
                      text: 'Something else',
                      btnClass: 'btn-blue',
                      keys: ['enter', 'shift'],
                      action: function(){
                          $.alert('Something else?');
                      }
                  }
                  */
              }
          });          
        }
        if(data.status==="validationError")
        {
            $(div).addClass('alert alert-danger');   
            for(let [key,value ] of Object.entries(data.msg))
            {
              //$("#addingResponse").addClass('alert alert-danger');
              $(div).append(`<li>${value}</li>`);
            }
            
        }

        if(data.status==="error")
        {
            $.alert({
                title: 'Failed',
                content: data.msg,
            });  

        }

      },
      error:()=>{
        $(".overlay").hide();
            $.alert({
                title: 'Failed',
                content: 'Something went wrong, please try again!',
            });  
      }
    });
  }
  function ajaxPost(url,formData,div)
  {
    $(div).removeClass('alert alert-danger');
    $(div).empty();
    $.ajax({
      url: url,
      type: 'post',
      data: formData,
      dataType: 'json',
      beforeSend:()=>{
        $(".overlay").show();
      },
      success:(data)=>{
        $(".overlay").hide();
        if(data.status==="ok")
        {
          $.confirm({
              title: 'Success!',
              content: data.msg,
              buttons: {
                  ok: function () {
                      location.reload();
                  },
                  /*
                  cancel: function () {
                      $.alert('Canceled!');
                  },

                  somethingElse: {
                      text: 'Something else',
                      btnClass: 'btn-blue',
                      keys: ['enter', 'shift'],
                      action: function(){
                          $.alert('Something else?');
                      }
                  }
                  */
              }
          });          
        }
        if(data.status==="validationError")
        {
            $(div).addClass('alert alert-danger');   
            for(let [key,value ] of Object.entries(data.msg))
            {
              //$("#addingResponse").addClass('alert alert-danger');
              $(div).append(`<li>${value}</li>`);
            }
            
        }

        if(data.status==="error")
        {
            $.alert({
                title: 'Failed',
                content: data.msg,
            });  

        }

      },
      error:()=>{
        $(".overlay").hide();
            $.alert({
                title: 'Failed',
                content: 'Something went wrong, please try again!',
            });  
      }
    });
  }

  function ajaxDelete(url,formData,whichtr)
  {

    $.confirm({
        title: 'Do you want to delete?',
        buttons: {
            confirm: function () {
              $.ajax({
                url: url,
                type: 'post',
                data: formData,
                dataType: 'html',
                beforeSend:()=>{
                  $(".overlay").show();
                },
                success:(data)=>{
                  $(".overlay").hide();
                  whichtr.remove();
                },
                error:()=>{
                  $(".overlay").hide();
                }      
              })
            },
            cancel: function () {
                
            },
        }
    });   

  }
